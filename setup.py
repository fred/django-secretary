#!/usr/bin/python3
"""Placeholder setup script. The configuration is in setup.cfg file."""

from distutils.command.build import build

from setuptools import setup


class custom_build(build):
    """Custom build class."""

    sub_commands = [
        ("compile_catalog", None),
    ] + build.sub_commands


setup(cmdclass={"build": custom_build})
