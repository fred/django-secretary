ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

0.7.0 (2024-08-06)
------------------

* Use UID in template admin (#68).
* Add template types to admin (#70).
* Add ``JinjaUndefinedMiddleware`` (#69).
* Add ``floatformat`` filter to ``DjangoL10nExtension`` (#72).
* Fix annotations.
* Update project setup.

0.6.0 (2024-05-30)
------------------

* Drop support for python 3.7.
* Add support for python 3.12.
* Drop support for Django 4.0.
* Drop support for Django 4.1.
* Add support for Django 4.2.
* Add support for Django 5.0.
* Add support for Jinja2 templates (#46, #49, #50, #51, #56, #57, #64, #65, #66, #67).
* Turn ``.utils.Template`` into ``TemplateWrapper`` (#45).
* Add the ability to have multiple preferred backends (#44, #63).
* Implement proper choice of engines (#54).
* Unify basic API in ``Protocol`` (#43).
* Add ``uid`` to the ``Template`` model (#48).
* Implement deletion from admin interface (#55).
* Fix doubleslashes in media path (#60).
* Add new states translations (#52).
* Fix annotations.
* Update project setup.

0.5.2 (2023-10-24)
------------------

* Update premium domain wording.
* Fix annotations.

0.5.1 (2023-05-11)
------------------

* Add new state flag descriptions (#37).

0.5.0 (2023-04-11)
------------------

* Add support for python 3.11.
* Add support for Django 4.1.
* Tweak administration (#33).
* Fix type annotations.
* Update project setup.

0.4.0 (2022-08-15)
------------------

* Add label to template collection (#31).
* Define auto fields.

0.3.0 (2022-07-21)
------------------

* Drop support for Django 2.2.
* Add floatformat filter override (#29).
* Add search fields to administration (#26).
* Update project setup.

0.2.1 (2022-05-30)
------------------

* Handle template filter errors (#21).
* Add ``forcewrap`` filter (#15).
* Log signer errors (#23).
* Fix default signer output (#24).
* Fix ``SECRETARY_STATE_FLAGS_DESCRIPTIONS`` setting (#25).
* Fix building of locales.

0.2.0 (2022-03-21)
------------------

* Drop support for python 3.6.
* Add support for python 3.10.
* Drop support for Django 3.0 and 3.1.
* Add support for Django 3.2 and 4.0.
* Add TTF mime type.
* Separate ``TemplateRendererMixin`` (#2).
* Add global assets (#3).
* Return the active template from ``get_active_template`` (#4).
* Let fetcher search assets by name (#4).
* Add action to activate templates to administration (#5).
* Turn constants module into a package (#11).
* Add DNSKEY algorithm flag and protocol enums (#11, #14).
* Add ``SECRETARY_STATE_FLAGS_DESCRIPTIONS`` setting (#11).
* Add template filters for parsing datetime and fred objects (#7, #9, #14).
* Add fred filters (#9).
* Add signer field to a template and signed PDF renderers (#12, #16).
* Add Czech locales (#8).
* Refactor renderer internals (#18, #19).
* Add intermediate ``Template`` object (#20).
* Add preferred backends for renderers (#17).
* Fix bugs in tests.
* Update static checks & project setup.
* Update mypy & fix annotations.

0.1.0 (2020-09-07)
------------------

Initial version.

* Simple service for managing and rendering templates.
