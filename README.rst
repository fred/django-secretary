================
Django-secretary
================

Django application for generating documents.

--------
Settings
--------

``SECRETARY_RENDERERS``
=======================

A list of renderers used for content negotiation for document rendering, see https://www.django-rest-framework.org/api-guide/content-negotiation/.
All values should be dotted paths of renderer classes for Django REST framework, see https://www.django-rest-framework.org/api-guide/renderers/.
Default value is ``('django_secretary.renderers.HtmlRenderer', 'django_secretary.renderers.TextRenderer', 'django_secretary.renderers.PdfRenderer')``.

``SECRETARY_SIGNERS``
=====================

A configuration of signers which sign the rendered templates.
Valid value is a mapping with an options described below and based on selected signature method.

``METHOD``
----------
Which method is used for signing, either ``command`` or ``pyhanko``.
Default is ``command``.

Command options
---------------

``COMMAND``
~~~~~~~~~~~
The command used to sign a PDF file.
A valid value is a list of strings.
A placeholder ``{output}`` may be used to provide a value of the ``OUTPUT`` option below.
The command is run in the temporary working directory with the input file under name ``input.pdf``.
This setting is required.

``ENVIRONMENT``
~~~~~~~~~~~~~~~
A mapping with environment variables used to run the signer.
Default value is ``None``.

``OUTPUT``
~~~~~~~~~~
A name of the output file.
Default value is ``input_signed.pdf``.

PyHanko options
---------------

``BORDER_WIDTH``
~~~~~~~~~~~~~~~~
A width of the border around the signature box.
Default is ``0``.

``BOX``
~~~~~~~
The position of signature box as a tuple of four integers.
See https://pyhanko.readthedocs.io/en/latest/api-docs/pyhanko.sign.html#pyhanko.sign.fields.SigFieldSpec.box for details.
Default is ``None``, i.e. no box.

``CERTIFICATE``
~~~~~~~~~~~~~~~
A path to PKCS12 certificate used to generate the signature.
This setting is required.

``FONT_FILE``
~~~~~~~~~~~~~
The name of custom font file for signature text.
See https://pyhanko.readthedocs.io/en/latest/cli-guide/config.html#styles-for-stamping-and-signature-appearances for details.
Default is ``None``, i.e. use pyHanko's default font.

``FONT_SIZE``
~~~~~~~~~~~~~
Font size of signature text.
Default is ``10``.

``IMAGE``
~~~~~~~~~
A path to a image used for visible signature.
Default is ``None``, i.e. no image.

``PASSWORD``
~~~~~~~~~~~~
A password to the certificate.
Default is ``None``, i.e. no password.

``TEXT``
~~~~~~~~
Visible signature text.
See https://pyhanko.readthedocs.io/en/latest/cli-guide/config.html#styles-for-stamping-and-signature-appearances for details.
Default is ``%(ts)s``.

``SECRETARY_STATE_FLAGS_DESCRIPTIONS``
======================================
A mapping of the state flags to their human readable descriptions.
Provided setting is merged to the default descriptions.
For default value see ``django_secretary.settings.DEFAULT_STATE_FLAGS_DESCRIPTIONS``.

``SECRETARY_TEMPLATES``
=======================

A list containing the settings for all template engines to be used for document
rendering. Uses the same syntax as Django ``TEMPLATES`` setting, see
https://docs.djangoproject.com/en/dev/ref/settings/#templates. Default value is
same as ``TEMPLATES``. It is highly recommended to define this setting
explicitly to prevent undesirable impact on rendering of the application pages.

Two backends are currently supported:
``django.template.backends.django.DjangoTemplates``, with our custom
``django_secretary.utils.templates.TemplateLoader``, and
``django.template.backends.jinja2.Jinja2``, with our
``django_secretary.utils.templates.JinjaTemplateLoader``. These are designed to
use the django-secretary templates, other backends and loaders may not work
properly.

The Django backend uses the ``content`` property of the template model as its
content, while the Jinja backend uses ``jinja_content``. A template can have
both types of content defined, or just one. If there are more backends defined,
they are tried one by one, until one succeeds (i.e., for example, it is a
Django backend, and the template also has a ``content`` field).

Further, a backend's ``NAME`` is also used to select a preferred backend for a
renderer. Then the preferred backends are tried first, the rest are tried only
if the preferred all fail.
