[tox]
minversion = 4.18.0
envlist =
    clear-coverage
    quality
    translations
    py38-django{42}{,-reverse}
    py39-django{42}{,-reverse}
    py310-django{42,50,51}{,-reverse}
    py311-django{42,50,51}{,-reverse}
    py312-django{50,51}{,-reverse}
    py313-django{51}{,-reverse}
    compute-coverage
skip_missing_interpreters = true

[testenv]
depends = clear-coverage
setenv =
    py38,py39,py310,py311,py312,py313: PYTHONWARNINGS = {env:PYTHONWARNINGS:all}
    DJANGO_SETTINGS_MODULE = django_secretary.tests.settings
    reverse: REVERSE = --reverse
passenv =
    CI*
extras =
    test
deps =
    coverage[toml]  # remove '[toml]' when Python 3.9 is no longer used
    django42: django==4.2.*
    django50: django==5.0.*
    django51: django==5.1.*
skip_install =
    coverage: true
commands =
    coverage run --parallel-mode --source=django_secretary --branch -m django test {env:REVERSE:} {posargs:django_secretary}
constrain_package_deps = true
use_frozen_constraints = true
uv_python_preference = only-system

[testenv:clear-coverage]
depends =
commands =
    coverage erase

[testenv:compute-coverage]
depends =
    py38-django{42}{,-reverse}
    py39-django{42}{,-reverse}
    py310-django{42,50,51}{,-reverse}
    py311-django{42,50,51}{,-reverse}
    py312-django{50,51}{,-reverse}
    py313-django{51}{,-reverse}
parallel_show_output = true
commands =
    coverage combine
    coverage report --show-missing --include=*/tests/* --fail-under=100
    coverage report --show-missing --omit=*/tests/* --fail-under=100

[testenv:quality]
depends =
deps =
    django==4.2.*  # Lowest supported version for migrations
extras =
    quality
    test
    types
# Do not fail on first error, but run all the checks
ignore_errors = true
commands =
    ruff check
    ruff format --check
    django-admin makemigrations django_secretary --check --noinput --dry-run --verbosity 3
    mypy django_secretary
    doc8 CHANGELOG.rst README.rst

[testenv:translations]
depends = quality
setenv =
    DJANGO_SETTINGS_MODULE =
allowlist_externals =
    msgcmp
    rm
# Do not fail on first error, but run all the checks
ignore_errors = true
extras = quality
changedir = {toxinidir}/django_secretary
commands =
    polint --show-msg locale/cs/LC_MESSAGES/django.po
    django-admin makemessages --locale C --no-obsolete --no-location --keep-pot
    msgcmp locale/cs/LC_MESSAGES/django.po locale/django.pot
    -rm -r locale/django.pot locale/C

[testenv:make-translations]
setenv =
    DJANGO_SETTINGS_MODULE =
allowlist_externals =
    msgattrib
extras = quality
changedir = {toxinidir}/django_secretary
commands =
    django-admin makemessages --locale cs --no-obsolete --no-location
    msgattrib --sort-output -o locale/cs/LC_MESSAGES/django.po locale/cs/LC_MESSAGES/django.po
    django-admin compilemessages
