"""URLs for django-secretary."""

from django.urls import include, path

from .views import ROUTER

urlpatterns = [
    path("api/", include(ROUTER.urls)),
]
