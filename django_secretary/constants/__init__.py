"""Various constants for django-secretary."""

from enum import Enum, unique

from django.utils.translation import gettext_lazy as _

from .utils import _DnskeyEnumBase, _DnskeyFlagBase


@unique
class MimeType(str, Enum):
    """Enum of mime types."""

    CSS = "text/css"
    SVG = "image/svg"
    PNG = "image/png"
    TTF = "application/x-font-ttf"


IMAGE_MIME_TYPES = (MimeType.SVG, MimeType.PNG)


# https://www.iana.org/assignments/dns-sec-alg-numbers/dns-sec-alg-numbers.xhtml
_DNSKEY_ALGORITHMS = {
    "DELETE_DS": (0, _("Delete DS")),
    "RSAMD5": (1, _("RSA/MD5 (deprecated, see 5)")),
    "DH": (2, _("Diffie-Helman")),
    "DSA": (3, _("DSA/SHA1")),
    "RSASHA1": (5, _("RSA/SHA-1")),
    "DSA_NSEC3_SHA1": (6, _("DSA-NSEC3-SHA1")),
    "RSASHA1_NSEC3_SHA1": (7, _("RSASHA1-NSEC3-SHA1")),
    "RSASHA256": (8, _("RSA/SHA-256")),
    "RSASHA512": (10, _("RSA/SHA-512")),
    "GOST": (12, _("GOST R 34.10-2001")),
    "ECDSAP256SHA256": (13, _("ECDSA Curve P-256 with SHA-256")),
    "ECDSAP384SHA384": (14, _("ECDSA Curve P-384 with SHA-384")),
    "ED25519": (15, _("Ed25519")),
    "ED448": (16, _("Ed448")),
    "INDIRECT": (252, _("Reserved for Indirect Keys")),
    "PRIVATEDNS": (253, _("Private algorithm")),
    "PRIVATEOID": (254, _("Private algorithm OID")),
}
# Add usassigned
_DNSKEY_ALGORITHMS.update({"UNASSIGNED_{}".format(i): (i, _("Unassigned")) for i in range(17, 123)})
# Add reserved
_DNSKEY_ALGORITHMS.update(
    {
        "RESERVED_{}".format(i): (i, _("Reserved"))
        for i in range(0, 256)
        if i not in [v[0] for v in _DNSKEY_ALGORITHMS.values()]
    }
)
DnskeyAlgorithm = _DnskeyEnumBase("DnskeyAlgorithm", _DNSKEY_ALGORITHMS)  # type: ignore[arg-type]


# https://www.iana.org/assignments/dnskey-flags/dnskey-flags.xhtml
_DNSKEY_FLAGS = {
    "ZONE": (1 << 8, _("ZONE")),
    "REVOKE": (1 << 7, _("REVOKE")),
    "SECURE_ENTRY_POINT": (1 << 0, _("Secure Entry Point (SEP)")),
}
# Add usassigned
_DNSKEY_FLAGS.update(
    {
        "UNASSIGNED_{}".format(15 - i): (1 << i, _("Unassigned"))
        for i in range(0, 16)
        if 1 << i not in [v[0] for v in _DNSKEY_FLAGS.values()]
    }
)
DnskeyFlag = _DnskeyFlagBase("DnskeyFlag", _DNSKEY_FLAGS)  # type: ignore[arg-type]


# https://www.iana.org/assignments/dns-key-rr/dns-key-rr.xhtml
_DNSKEY_PROTOCOLS = {
    "DNSSEC": (3, _("DNSSEC")),
}
# Add usassigned
_DNSKEY_PROTOCOLS.update({"UNASSIGNED_{}".format(i): (i, _("Unassigned")) for i in range(5, 255)})
# Add reserved
_DNSKEY_PROTOCOLS.update(
    {
        "RESERVED_{}".format(i): (i, _("Reserved"))
        for i in (1, 2, 4, 255)
        if i not in [v[0] for v in _DNSKEY_PROTOCOLS.values()]
    }
)
DnskeyProtocol = _DnskeyEnumBase("DnskeyProtocol", _DNSKEY_PROTOCOLS)  # type: ignore[arg-type]
