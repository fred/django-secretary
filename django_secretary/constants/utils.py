"""Utilities for constants."""

from enum import Enum, IntFlag, unique
from typing import Set, Tuple, cast


class _DnskeyMixin:
    """Base mixin for DNS key enums and flags."""

    label: str

    # mypy doesn't detect enum items, if generated dynamically.
    # Define __getattribute__ to mask that.
    def __getattribute__(self, name: str) -> "_DnskeyMixin":
        return cast("_DnskeyMixin", super().__getattribute__(name))


@unique
class _DnskeyEnumBase(_DnskeyMixin, int, Enum):
    """Base class for DnskeyAlgorithm."""

    def __new__(cls, value: int, label: str) -> "_DnskeyEnumBase":
        """Construct item with label."""
        obj = super().__new__(cls, value)
        obj._value_ = value

        obj.label = label
        return obj


@unique
class _DnskeyFlagBase(_DnskeyMixin, IntFlag):  # type: ignore[misc]
    """Base class for DnskeyFlag."""

    def __new__(cls, value: int, label: str = "") -> "_DnskeyFlagBase":
        """Construct item with label."""
        obj = int.__new__(cls, value)
        obj._value_ = value

        obj.label = label
        return obj

    @property
    def flags(self) -> Set["_DnskeyFlagBase"]:
        """Return individual flags."""
        return {f for f in cast(Tuple["_DnskeyFlagBase", ...], tuple(self.__class__)) if f in self}
