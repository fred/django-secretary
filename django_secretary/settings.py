"""Application settings for django-secretary."""

from functools import partial
from typing import Any, Dict, Mapping, Sequence, cast

from appsettings import AppSettings, CallablePathSetting, DictSetting, NestedListSetting
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from rest_framework.renderers import BaseRenderer

DEFAULT_STATE_FLAGS_DESCRIPTIONS = {
    "deleteCandidate": _("To be deleted"),
    "linked": _("Has relation to other records in the registry"),
    "serverBlocked": _("Administratively blocked"),
    "conditionallyIdentifiedContact": _("Contact is conditionally identified"),
    "identifiedContact": _("Contact is identified"),
    "validatedContact": _("Contact is validated"),
    "serverDeleteProhibited": _("Deletion forbidden"),
    "serverTransferProhibited": _("Sponsoring registrar change forbidden"),
    "serverUpdateProhibited": _("Update forbidden"),
    "serverRegistrantChangeProhibited": _("Registrant change forbidden"),
    "serverRenewProhibited": _("Registration renewal forbidden"),
    "expirationWarning": _("The domain expires in 30 days"),
    "deleteWarning": _("The domain will be deleted in 11 days"),
    "validationWarning1": _("The domain validation expires in 30 days"),
    "validationWarning2": _("The domain validation expires in 15 days"),
    "unguarded": _("The domain is 30 days after expiration"),
    "outzoneUnguarded": _("The domain is out of zone after 30 days in expiration state"),
    "nssetMissing": _("The domain doesn't have associated nsset"),
    "expired": _("Domain expired"),
    "outzone": _("The domain isn't generated in the zone"),
    "serverOutzoneManual": _("The domain is administratively kept out of zone"),
    "serverInzoneManual": _("The domain is administratively kept in zone"),
    "notValidated": _("Domain not validated"),
    "outzoneUnguardedWarning": _("The domain is to be out of zone soon."),
    "serverContactNameChangeProhibited": _("Name update forbidden"),
    "serverContactOrganizationChangeProhibited": _("Organization update forbidden"),
    "serverContactIdentChangeProhibited": _("Ident update forbidden"),
    "serverContactPermanentAddressChangeProhibited": _("Permanent address update forbidden"),
    "serverLinkProhibited": _("Contact linking forbidden"),
    "premiumDomain": _("VIP domain"),
}


class MergeDictSetting(DictSetting):
    """Merge dict setting."""

    def transform(self, value: Dict) -> Dict:
        """Merge setting value with the defaults."""
        result = cast(Dict, self.default.copy())
        result.update(value)
        return result


class SignersSetting(DictSetting):
    """Custom setting for SIGNERS."""

    SIGNER_DEFAULTS: Dict[str, Any] = {
        # Default method
        "METHOD": "command",
        # Defaults for command
        "ENVIRONMENT": None,
        "OUTPUT": "input_signed.pdf",
        # Defaults for pyhanko
        "BORDER_WIDTH": 0,
        "FONT_FILE": None,
        "FONT_SIZE": 10,
        "IMAGE": None,
        "PASSWORD": None,
        "TEXT": "%(ts)s",
    }

    def _validate_pyhanko_signer(self, signer: Dict[str, Any]) -> None:
        if "CERTIFICATE" not in signer:
            raise ValidationError("CERTIFICATE option is required for pyhanko method.")
        if "BOX" in signer:
            box = signer["BOX"]
            if not isinstance(box, tuple) or len(box) != 4 or not all(isinstance(i, int) for i in box):
                raise ValidationError("BOX has to be a tuple of 4 integers.")
        if "IMAGE" in signer and not isinstance(signer["IMAGE"], str):
            raise ValidationError("IMAGE has to be a string.")

    def _validate_command_signer(self, signer: Dict[str, Any]) -> None:
        if "COMMAND" not in signer:
            raise ValidationError("COMMAND option is required for command method.")
        if not isinstance(signer["COMMAND"], (list, tuple)) or not all(isinstance(i, str) for i in signer["COMMAND"]):
            raise ValidationError("COMMAND has to be a sequence of strings.")
        if "ENVIRONMENT" in signer and not isinstance(signer["ENVIRONMENT"], Mapping):
            raise ValidationError("ENVIRONMENT has to be a mapping.")
        if "OUTPUT" in signer and not isinstance(signer["OUTPUT"], str):
            raise ValidationError("OUTPUT has to be a string.")

    def validate(self, value: Dict[str, Dict]) -> None:
        """Check structure of signer settings."""
        for signer in value.values():
            method = signer.get("METHOD", "command")
            if method == "command":
                self._validate_command_signer(signer)
            elif method == "pyhanko":
                self._validate_pyhanko_signer(signer)
            else:
                raise ValidationError("Unknown method '{}'.".format(method))

    def transform(self, value: Dict[str, Dict]) -> Dict[str, Dict]:
        """Set defaults for signer settings."""
        return {k: dict(self.SIGNER_DEFAULTS, **v) for k, v in value.items()}


def _get_default_renderers() -> Sequence[BaseRenderer]:
    # Utility function to avoid circular imports.
    from .renderers import HtmlRenderer, PdfRenderer, TextRenderer

    return (HtmlRenderer, TextRenderer, PdfRenderer)


class SecretaryAppSettings(AppSettings):
    """Django-secretary application settings."""

    RENDERERS = NestedListSetting(inner_setting=CallablePathSetting(), default=_get_default_renderers)
    SIGNERS = SignersSetting(key_type=str, value_type=dict)
    TEMPLATES = NestedListSetting(
        inner_setting=DictSetting(key_type=str), default=partial(getattr, settings, "TEMPLATES")
    )
    STATE_FLAGS_DESCRIPTIONS = MergeDictSetting(key_type=str, default=DEFAULT_STATE_FLAGS_DESCRIPTIONS)

    class Meta:
        setting_prefix = "SECRETARY_"


SECRETARY_SETTINGS = SecretaryAppSettings()
