"""Register django_secretary to django admin."""

from copy import deepcopy
from typing import Any, Dict, Optional, Sequence, Tuple, Type, Union, cast

from django import forms
from django.contrib.admin import (
    EmptyFieldListFilter,
    ModelAdmin,
    TabularInline,
    display,
    site,
)
from django.contrib.messages import ERROR
from django.core.files.base import ContentFile
from django.db import models
from django.db.models import Count
from django.db.models.fields import BLANK_CHOICE_DASH
from django.db.models.fields.files import FieldFile
from django.db.models.query import QuerySet
from django.db.transaction import atomic
from django.http import HttpRequest
from django.utils.translation import gettext_lazy as _

from .models import Asset, Tag, Template, TemplateCollection
from .settings import SECRETARY_SETTINGS


class TextFileField(forms.CharField):
    """Form field which renders file content as a text."""

    widget = forms.Textarea

    def __init__(self, *, strip: bool = False, **kwargs: Any) -> None:
        # Disable stripping by default and ignore any provided max_length.
        kwargs.pop("max_length", None)
        super().__init__(strip=strip, **kwargs)

    def prepare_value(self, value: Optional[Union[str, FieldFile]]) -> Optional[str]:
        """Prepare value for rendering in widget."""
        # Model provides `FieldFile`, form provides `str`
        if isinstance(value, FieldFile):
            if not value:  # File objects return `False` when empty.
                return None
            return cast(bytes, value.read()).decode()
        return value

    def to_python(self, data: Optional[str]) -> Optional[ContentFile]:
        """Convert value from widget to python."""
        data = super().to_python(data)
        if not data:
            return None
        # models.FileField requires file to have a name.
        return ContentFile(data, name="dummy.txt")

    def has_changed(self, initial: Optional[FieldFile], data: Optional[str]) -> bool:
        """Return True if data differs from initial."""
        if self.disabled:
            return False
        initial_content = self.prepare_value(initial)
        data_content = data or None
        return initial_content != data_content


def _get_signers() -> Sequence[Tuple[str, str]]:
    return BLANK_CHOICE_DASH + [(k, k) for k in SECRETARY_SETTINGS.SIGNERS]


class TemplateForm(forms.ModelForm):
    """Form for a template model."""

    signer = forms.ChoiceField(required=False, choices=_get_signers)

    def clean(self) -> Dict[str, Any]:
        """Modify cleaned data to prevent creation of a new file if the content hasn't changed."""
        cleaned_data: Dict[str, Any] = self.cleaned_data
        if "content" not in self.changed_data:
            # If the file hasn't changed, set its value to None to signal models.FileField it hasn't changed.
            cleaned_data["content"] = None
        elif "content" not in cleaned_data or not cleaned_data["content"]:
            # If the file content changed to '', set its value to False to signal models.FileField it is to be cleared.
            # https://github.com/django/django/blob/b287af5dc954628d4b336aefc5027b2edceee64b/django/db/models/fields/files.py#L340
            cleaned_data["content"] = False
        if "jinja_content" not in self.changed_data:
            # If the file hasn't changed, set its value to None to signal models.FileField it hasn't changed.
            cleaned_data["jinja_content"] = None
        elif "jinja_content" not in cleaned_data or not cleaned_data["jinja_content"]:
            # If the file content changed to '', set its value to False to signal models.FileField it is to be cleared.
            # https://github.com/django/django/blob/b287af5dc954628d4b336aefc5027b2edceee64b/django/db/models/fields/files.py#L340
            cleaned_data["jinja_content"] = False
        return cleaned_data


class AssetInline(TabularInline):
    """Inline admin for assets."""

    model = Asset
    formfield_overrides = {
        models.TextField: {"widget": forms.TextInput},
    }


class AssetAdmin(ModelAdmin):
    """Model admin for assets."""

    list_display = ("name", "template", "uuid")
    search_fields = ("name",)
    formfield_overrides = {
        models.TextField: {"widget": forms.TextInput},
    }


class TagAdmin(ModelAdmin):
    """Model admin for tags."""

    list_display = ("name",)
    formfield_overrides = {
        models.TextField: {"widget": forms.TextInput},
    }


class TemplateAdmin(ModelAdmin):
    """Model admin for templates."""

    list_display = ("name", "is_active", "create_datetime", "uid", "has_content", "has_jinja_content")
    list_filter = ("is_active", ("content", EmptyFieldListFilter), ("jinja_content", EmptyFieldListFilter))
    search_fields = ("name",)
    inlines = (AssetInline,)
    form = TemplateForm
    formfield_overrides = {
        models.TextField: {"widget": forms.TextInput},
        models.FileField: {"form_class": TextFileField, "widget": forms.Textarea(attrs={"cols": 80, "rows": 15})},
    }
    actions = ["make_active"]

    @display(description=_("content"), boolean=True)
    def has_content(self, obj: Template) -> bool:
        """Return whether obj has content filled."""
        return bool(obj.content)

    @display(description=_("Jinja content"), boolean=True)
    def has_jinja_content(self, obj: Template) -> bool:
        """Return whether obj has jinja_content filled."""
        return bool(obj.jinja_content)

    def make_active(self, request: HttpRequest, queryset: QuerySet) -> None:
        """Make selected templates active (admin action)."""
        with atomic():
            # Ordering need to be reset and values limited to 'name' to get a correct SQL.
            duplicates = queryset.order_by().values("name").annotate(count=Count("name")).filter(count__gte=2)
            if duplicates:
                # Duplicates found, abort!
                self.message_user(
                    request, "Template {} selected multiple times.".format(duplicates[0]["name"]), level=ERROR
                )
                return
            deact_query = self.get_queryset(request).filter(
                name__in=queryset.values_list("name", flat=True), is_active=True
            )
            deact_query = deact_query.exclude(pk__in=queryset.values_list("pk", flat=True))
            deactivated = deact_query.update(is_active=False)
            activated = queryset.filter(is_active=False).update(is_active=True)
            self.message_user(
                request, "{} templates marked as active and {} as inactive.".format(activated, deactivated)
            )

    make_active.short_description = _("Activate selected templates")  # type: ignore[attr-defined]
    make_active.allowed_permissions = ("change",)  # type: ignore[attr-defined]

    def get_form(
        self,
        request: HttpRequest,
        obj: Optional[Template] = None,
        change: bool = False,
        **kwargs: Any,
    ) -> Type[TemplateForm]:
        """Add current signer to choices if not present."""
        form = cast(Type[TemplateForm], super().get_form(request, obj, change, **kwargs))
        if obj and obj.signer is not None and obj.signer not in [k for k, v in _get_signers()]:
            signer_copy = deepcopy(form.base_fields["signer"])  # type: ignore[attr-defined]
            signer_copy.choices = list(signer_copy.choices) + [(obj.signer, obj.signer)]
            form.base_fields["signer"] = signer_copy  # type: ignore[attr-defined]
        return form


class TemplateCollectionAdmin(ModelAdmin):
    """Model admin for template collections."""

    list_display = ("name", "label", "tags_display")
    list_filter = ("tags",)
    filter_horizontal = ("tags",)
    search_fields = ("name", "label")
    formfield_overrides = {
        models.TextField: {"widget": forms.TextInput},
    }

    @display(description=_("Tags"))
    def tags_display(self, obj: TemplateCollection) -> str:
        """Return a string representation of tags to display."""
        return ", ".join(t.name for t in obj.tags.all())


site.register(Asset, AssetAdmin)
site.register(Tag, TagAdmin)
site.register(Template, TemplateAdmin)
site.register(TemplateCollection, TemplateCollectionAdmin)
