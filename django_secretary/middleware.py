"""Custom middlewares."""

import json
from typing import Optional

from django.http import HttpRequest, HttpResponse, HttpResponseBadRequest
from django.utils.deprecation import MiddlewareMixin
from jinja2 import UndefinedError


class JinjaUndefinedMiddleware(MiddlewareMixin):
    """Middleware which handles Undefined from jinja templates."""

    def process_exception(self, request: HttpRequest, exception: Exception) -> Optional[HttpResponse]:
        """Capture UndefinedError from jinja and return Bad response instead."""
        if isinstance(exception, UndefinedError):
            response = HttpResponseBadRequest(json.dumps({"error": str(exception)}), content_type="application/json")
            return response
        return None
