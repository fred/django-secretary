"""Protocols defining function of templates."""

from typing import Any, Dict, Optional, Protocol, Union

from django.http.request import HttpRequest


class Origin(Protocol):
    """A protocol for an origin of a template."""

    name: str
    template_name: Union[str, bytes, None]


class Template(Protocol):
    """The engine-level template object.

    This is compatible with `django.template.backends.django.Template` and
    `django.template.backends.jinja2.Template`, as well as our
    `django_secretary.utils.Template`.
    """

    def render(self, context: Optional[Dict[str, Any]] = None, request: Optional[HttpRequest] = None) -> str:
        """Render the template with a given context."""


class Engine(Protocol):
    """A template engine, i.e. a specific instance of a `django.template.backends.Backend`, with configured options."""

    def get_template(self, template_name: str) -> Optional[Template]:
        """Return a template object for a given name."""
