from django.test import SimpleTestCase

from django_secretary.constants import DnskeyAlgorithm, DnskeyFlag, DnskeyProtocol


class DnskeyAlgorithmTest(SimpleTestCase):
    def test_props(self):
        self.assertEqual(DnskeyAlgorithm.DELETE_DS.name, "DELETE_DS")
        self.assertEqual(DnskeyAlgorithm.DELETE_DS.value, 0)
        self.assertEqual(DnskeyAlgorithm.DELETE_DS.label, "Delete DS")


class DnskeyFlagTest(SimpleTestCase):
    def test_props(self):
        self.assertEqual(DnskeyFlag.ZONE.name, "ZONE")
        self.assertEqual(DnskeyFlag.ZONE.value, 256)
        self.assertEqual(DnskeyFlag.ZONE.label, "ZONE")

    def test_flags(self):
        dnskey = DnskeyFlag.ZONE | DnskeyFlag.REVOKE | DnskeyFlag.UNASSIGNED_2  # type: ignore[operator]
        flags = dnskey.flags
        self.assertEqual(flags, {DnskeyFlag.ZONE, DnskeyFlag.REVOKE, DnskeyFlag.UNASSIGNED_2})


class DnskeyProtocolTest(SimpleTestCase):
    def test_props(self):
        self.assertEqual(DnskeyProtocol.DNSSEC.name, "DNSSEC")
        self.assertEqual(DnskeyProtocol.DNSSEC.value, 3)
        self.assertEqual(DnskeyProtocol.DNSSEC.label, "DNSSEC")
