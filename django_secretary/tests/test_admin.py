from typing import Type, cast
from unittest.mock import Mock, sentinel

from django.contrib.auth.models import User
from django.contrib.messages import get_messages
from django.core.files.base import ContentFile
from django.db.models.fields import BLANK_CHOICE_DASH
from django.db.models.fields.files import FieldFile
from django.test import SimpleTestCase, TestCase, override_settings
from django.urls import reverse
from testfixtures import TempDirectory

from django_secretary.admin import TemplateAdmin, TemplateCollectionAdmin, TemplateForm, TextFileField
from django_secretary.models import Tag, Template, TemplateCollection

from .utils import create_template


def _field_file_mock(content: bytes) -> FieldFile:
    """Mock field file."""
    field_file = FieldFile(instance=Mock(), field=Mock(), name="dummy.txt")
    field_file.file = ContentFile(content, name="dummy.txt")
    return field_file


class TextFileFieldTest(SimpleTestCase):
    def test_strip_disabled(self):
        field = TextFileField()
        self.assertFalse(field.strip)

    def test_max_length_ignored(self):
        field = TextFileField(max_length=10)
        # Check it doesn't raise error
        value = "Really long value"
        result = field.clean(value)
        self.assertEqual(result.read(), value)

    def test_prepare_value(self):
        field = TextFileField()
        test_data = (
            # value, result
            (None, None),
            ("Gazpacho!", "Gazpacho!"),
            (_field_file_mock(b"Gazpacho!"), "Gazpacho!"),
        )
        for value, result in test_data:
            with self.subTest(value=value, result=result):
                self.assertEqual(field.prepare_value(value), result)

    def test_to_python_empty(self):
        field = TextFileField()
        test_data = (
            # data, result
            (None, None),
            ("", None),
        )
        for data, result in test_data:
            with self.subTest(data=data, result=result):
                self.assertEqual(field.to_python(data), result)

    def test_to_python(self):
        field = TextFileField()
        result = field.to_python("Gazpacho!")

        result = cast(ContentFile, result)
        self.assertEqual(result.read(), "Gazpacho!")
        self.assertEqual(result.name, "dummy.txt")

    def test_has_changed(self):
        field = TextFileField()
        test_data = (
            # initial, data, result
            (None, None, False),
            (None, "", False),
            (_field_file_mock(b"Gazpacho!"), "Gazpacho!", False),
            (None, "Gazpacho!", True),
            (_field_file_mock(b"Gazpacho!"), "Quagaars!", True),
            (_field_file_mock(b"Gazpacho!"), "", True),
        )
        for initial, data, result in test_data:
            with self.subTest(initial=initial, data=data, result=result):
                self.assertEqual(field.has_changed(initial, data), result)

    def test_has_changed_disabled(self):
        field = TextFileField(disabled=True)
        self.assertFalse(field.has_changed(sentinel.initial, sentinel.data))


class TestTemplateForm(TemplateForm):
    """Template form for testing."""

    content = TextFileField()

    class Meta:
        model = Template
        fields = ("content",)


class TestJinjaTemplateForm(TemplateForm):
    """Template form for testing."""

    jinja_content = TextFileField()

    class Meta:
        model = Template
        fields = ("jinja_content",)


class TemplateFormTest(SimpleTestCase):
    content_field = "content"
    template_form_cls: Type[TemplateForm] = TestTemplateForm

    def test_clean_new(self):
        form = self.template_form_cls({self.content_field: "Gazpacho!"})
        form.is_valid()
        self.assertEqual(form.cleaned_data[self.content_field].read(), "Gazpacho!")

    def test_clean_no_change(self):
        instance = Template(**{self.content_field: ContentFile(b"Gazpacho!", name="dummy.txt")})
        form = self.template_form_cls({self.content_field: "Gazpacho!"}, instance=instance)
        form.is_valid()
        self.assertIsNone(form.cleaned_data[self.content_field])

    def test_clean_change(self):
        instance = Template(**{self.content_field: ContentFile(b"Gazpacho!", name="dummy.txt")})
        form = self.template_form_cls({self.content_field: "Quagaars!"}, instance=instance)
        form.is_valid()
        self.assertEqual(form.cleaned_data[self.content_field].read(), "Quagaars!")

    def test_clean_delete(self):
        instance = Template(**{self.content_field: ContentFile(b"Gazpacho!", name="dummy.txt")})
        form = self.template_form_cls({self.content_field: ""}, instance=instance)
        form.is_valid()
        self.assertFalse(form.cleaned_data[self.content_field])


class JinjaTemplateFormTest(TemplateFormTest):
    content_field = "jinja_content"
    template_form_cls = TestJinjaTemplateForm


@override_settings(ROOT_URLCONF="django_secretary.tests.urls")
class TemplateAdminTest(TestCase):
    def setUp(self) -> None:
        # Use temporary file storage
        tmp_dir = TempDirectory()
        cast(TestCase, self).addCleanup(tmp_dir.cleanup)
        media_patcher = cast(TestCase, self).settings(MEDIA_ROOT=tmp_dir.path, MEDIA_URL="/media/")
        cast(TestCase, self).addCleanup(media_patcher.disable)
        media_patcher.enable()

        self.user = User.objects.create_superuser(username="rimmer", email="", password="")
        self.client.force_login(self.user)

    def test_has_content(self):
        admin = TemplateAdmin(Template, sentinel.site)
        django_template = create_template(name="django.txt", is_active=True, content="Gazpacho!")
        jinja_template = create_template(name="jinja.txt", is_active=True, jinja_content="Gazpacho!")
        both_template = create_template(name="both.txt", is_active=True, content="Gazpacho!", jinja_content="Gazpacho!")

        data = (
            (django_template, True),
            (jinja_template, False),
            (both_template, True),
        )
        for template, result in data:
            with self.subTest(template=template):
                self.assertEqual(admin.has_content(template), result)

    def test_has_jinja_content(self):
        admin = TemplateAdmin(Template, sentinel.site)
        django_template = create_template(name="django.txt", is_active=True, content="Gazpacho!")
        jinja_template = create_template(name="jinja.txt", is_active=True, jinja_content="Gazpacho!")
        both_template = create_template(name="both.txt", is_active=True, content="Gazpacho!", jinja_content="Gazpacho!")

        data = (
            (django_template, False),
            (jinja_template, True),
            (both_template, True),
        )
        for template, result in data:
            with self.subTest(template=template):
                self.assertEqual(admin.has_jinja_content(template), result)

    def test_make_active_active(self):
        template = create_template(name="example.txt", is_active=True, content="")
        data = {"action": "make_active", "_selected_action": [template.pk]}
        response = self.client.post(reverse("admin:django_secretary_template_changelist"), data)
        self.assertRedirects(response, reverse("admin:django_secretary_template_changelist"))
        # Check template stayed active.
        self.assertQuerySetEqual(Template.objects.values_list("is_active"), [(True,)], transform=tuple)
        self.assertEqual(
            tuple(str(m) for m in get_messages(response.wsgi_request)),
            ("0 templates marked as active and 0 as inactive.",),
        )

    def test_make_active_inactive(self):
        template = create_template(name="example.txt", is_active=False, content="")
        data = {"action": "make_active", "_selected_action": [template.pk]}
        response = self.client.post(reverse("admin:django_secretary_template_changelist"), data)
        self.assertRedirects(response, reverse("admin:django_secretary_template_changelist"))
        # Check template activated.
        self.assertQuerySetEqual(Template.objects.values_list("is_active"), [(True,)], transform=tuple)
        self.assertEqual(
            tuple(str(m) for m in get_messages(response.wsgi_request)),
            ("1 templates marked as active and 0 as inactive.",),
        )

    def test_make_active_switch(self):
        template_1 = create_template(name="example.txt", is_active=True, content="")
        template_2 = create_template(name="example.txt", is_active=False, content="")
        data = {"action": "make_active", "_selected_action": [template_2.pk]}
        response = self.client.post(reverse("admin:django_secretary_template_changelist"), data)
        self.assertRedirects(response, reverse("admin:django_secretary_template_changelist"))
        # Check active template switched.
        self.assertQuerySetEqual(
            Template.objects.values_list("pk", "is_active"),
            [(template_1.pk, False), (template_2.pk, True)],
            transform=tuple,
            ordered=False,
        )
        self.assertEqual(
            tuple(str(m) for m in get_messages(response.wsgi_request)),
            ("1 templates marked as active and 1 as inactive.",),
        )

    def test_make_active_fail(self):
        template_1 = create_template(name="example.txt", is_active=False, content="")
        template_2 = create_template(name="example.txt", is_active=False, content="")
        data = {"action": "make_active", "_selected_action": [template_1.pk, template_2.pk]}
        response = self.client.post(reverse("admin:django_secretary_template_changelist"), data)
        self.assertRedirects(response, reverse("admin:django_secretary_template_changelist"))
        # Check nothing changed.
        self.assertQuerySetEqual(
            Template.objects.values_list("pk", "is_active"),
            [(template_1.pk, False), (template_2.pk, False)],
            transform=tuple,
            ordered=False,
        )
        self.assertEqual(
            tuple(str(m) for m in get_messages(response.wsgi_request)),
            ("Template example.txt selected multiple times.",),
        )

    def test_add_view_form(self):
        with override_settings(SECRETARY_SIGNERS={"holly": {"COMMAND": ["sign"]}}):
            response = self.client.get(reverse("admin:django_secretary_template_add"))

            self.assertContains(response, "Add template")
            self.assertEqual(
                list(response.context["adminform"].form.fields["signer"].choices),
                BLANK_CHOICE_DASH + [("holly", "holly")],
            )

    def test_change_view_form(self):
        template = create_template(name="example.txt", is_active=False, content="")

        with override_settings(SECRETARY_SIGNERS={"holly": {"COMMAND": ["sign"]}}):
            response = self.client.get(
                reverse("admin:django_secretary_template_change", kwargs={"object_id": template.pk})
            )

            self.assertContains(response, "Change template")
            self.assertEqual(
                list(response.context["adminform"].form.fields["signer"].choices),
                BLANK_CHOICE_DASH + [("holly", "holly")],
            )

    def test_change_view_form_unknown_signer(self):
        template = create_template(name="example.txt", is_active=False, content="", signer="unknown")

        with override_settings(SECRETARY_SIGNERS={"holly": {"COMMAND": ["sign"]}}):
            response = self.client.get(
                reverse("admin:django_secretary_template_change", kwargs={"object_id": template.pk})
            )

            self.assertContains(response, "Change template")
            self.assertEqual(
                list(response.context["adminform"].form.fields["signer"].choices),
                BLANK_CHOICE_DASH + [("holly", "holly"), ("unknown", "unknown")],
            )


class TemplateCollectionAdminTest(TestCase):
    def test_tags_display_empty(self):
        admin = TemplateCollectionAdmin(TemplateCollection, sentinel.site)
        collection = TemplateCollection.objects.create(name="Androids")

        self.assertEqual(admin.tags_display(collection), "")

    def test_tags_display(self):
        admin = TemplateCollectionAdmin(TemplateCollection, sentinel.site)
        collection = TemplateCollection.objects.create(name="Androids")
        tag = Tag.objects.create(name="Toilet")
        collection.tags.add(tag)

        self.assertEqual(admin.tags_display(collection), "Toilet")

    def test_tags_display_multiple(self):
        admin = TemplateCollectionAdmin(TemplateCollection, sentinel.site)
        collection = TemplateCollection.objects.create(name="Androids")
        tag = Tag.objects.create(name="Toilet")
        collection.tags.add(tag)
        tag = Tag.objects.create(name="Pilot")
        collection.tags.add(tag)

        self.assertEqual(admin.tags_display(collection), "Toilet, Pilot")
