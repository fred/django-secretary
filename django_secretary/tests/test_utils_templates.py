from threading import local
from typing import cast
from unittest.mock import Mock, patch, sentinel
from uuid import UUID

from django.template import Engine, Origin, TemplateDoesNotExist, TemplateSyntaxError
from django.template.backends.django import DjangoTemplates, Template as EngineTemplate
from django.template.backends.jinja2 import Jinja2
from django.test import TestCase
from jinja2.environment import Environment
from testfixtures import TempDirectory

from django_secretary.models import Template as TemplateModel
from django_secretary.utils.templates import (
    JinjaTemplateLoader,
    TemplateLoader,
    TemplateWrapper,
    _get_template,
    get_active_template,
    override_template,
    secretary_environment,
)

from .utils import RenderTestMixin, create_template


class TestGetActiveTemplate(TestCase):
    def setUp(self):
        patcher = patch("django_secretary.utils.templates._ACTIVE_TEMPLATES", local())
        self.addCleanup(patcher.stop)
        self._active_templates = patcher.start()

    def test_no_active(self):
        # Test no active template
        TemplateModel.objects.create(name="example.html", is_active=True)
        with self.assertRaisesMessage(TemplateDoesNotExist, "There is no active template."):
            get_active_template()

    def test_no_active_name(self):
        # Test no active template with the name.
        template = TemplateModel.objects.create(name="example.html", is_active=True)
        self.assertEqual(get_active_template("example.html"), template)

    def test_no_active_not_found(self):
        # Test no active template
        with self.assertRaisesMessage(TemplateDoesNotExist, "'unknown.html' is neither active, nor found in database."):
            get_active_template("unknown.html")

    def test_active(self):
        self._active_templates.template = sentinel.template
        self.assertEqual(get_active_template(), sentinel.template)

    def test_active_name(self):
        self._active_templates.templates = {"example.html": sentinel.template}
        self.assertEqual(get_active_template("example.html"), sentinel.template)

    def test_active_unknown(self):
        self._active_templates.templates = {"example.html": sentinel.template}
        with self.assertRaisesMessage(TemplateDoesNotExist, "'unknown.html' has no active template."):
            get_active_template("unknown.html")


class TestOverrideTemplate(TestCase):
    def setUp(self):
        patcher = patch("django_secretary.utils.templates._ACTIVE_TEMPLATES", local())
        self.addCleanup(patcher.stop)
        self._active_templates = patcher.start()

    def test_override(self):
        # Test override correctly changes template there and back
        template = TemplateModel(name="example.html")
        with override_template(template):
            overriden = get_active_template()
            overriden_name = get_active_template("example.html")

        self.assertEqual(overriden, template)
        self.assertEqual(overriden_name, template)
        # Check template is deactivated when override exited.
        self.assertRaises(TemplateDoesNotExist, get_active_template)
        self.assertRaises(TemplateDoesNotExist, get_active_template, "example.html")

    def test_override_raise(self):
        # Test override correctly changes template back even if error occurs
        template = TemplateModel(name="example.html")
        with self.assertRaisesMessage(Exception, "ERROR_IN_OVERRIDE"):
            with override_template(template):
                raise Exception("ERROR_IN_OVERRIDE")

        # Check template is deactivated when override exited.
        self.assertRaises(TemplateDoesNotExist, get_active_template)  # type: ignore[unreachable]
        self.assertRaises(TemplateDoesNotExist, get_active_template, "example.html")

    def test_override_nested(self):
        # Test override correctly changes template there and back, even when nested
        template = TemplateModel(name="example.html")
        nested_template = TemplateModel(name="example.html")
        with override_template(template):
            overriden = get_active_template()
            overriden_name = get_active_template("example.html")
            with override_template(nested_template):
                overriden_nested = get_active_template()
                overriden_nested_name = get_active_template("example.html")
            returned = get_active_template()
            returned_name = get_active_template("example.html")

        self.assertEqual(overriden, template)
        self.assertEqual(overriden_name, template)
        self.assertEqual(overriden_nested, nested_template)
        self.assertEqual(overriden_nested_name, nested_template)
        self.assertEqual(returned, template)
        self.assertEqual(returned_name, template)
        # Check template is deactivated when override exited.
        self.assertRaises(TemplateDoesNotExist, get_active_template, "example.html")

    def test_override_parent(self):
        # Test override correctly changes also parent template
        parent = TemplateModel(name="parent.html")
        template = TemplateModel(name="example.html", parent=parent)
        with override_template(template):
            overriden = get_active_template("example.html")
            overriden_parent = get_active_template("parent.html")

        self.assertEqual(overriden, template)
        self.assertEqual(overriden_parent, parent)
        # Check templates are deactivated when override exited.
        self.assertRaises(TemplateDoesNotExist, get_active_template, "example.html")
        self.assertRaises(TemplateDoesNotExist, get_active_template, "parent.html")


class TestTemplateLoader(TestCase):
    engine = Engine()

    def setUp(self):
        patcher = patch("django_secretary.utils.templates._ACTIVE_TEMPLATES", local())
        self.addCleanup(patcher.stop)
        self._active_templates = patcher.start()

        # Use temporary file storage
        tmp_dir = TempDirectory()
        self.addCleanup(tmp_dir.cleanup)
        storage_patcher = self.settings(MEDIA_ROOT=tmp_dir.path)
        self.addCleanup(storage_patcher.disable)
        storage_patcher.enable()
        self.template = create_template(name="example.html", is_active=True, content="Something completely different")

    def test_get_template_sources(self):
        loader = TemplateLoader(self.engine)
        origin = Origin(name=sentinel.name, loader=loader)
        self.assertCountEqual(loader.get_template_sources(sentinel.name), (origin,))

    def test_get_contents(self):
        loader = TemplateLoader(self.engine)
        origin = Origin(name="example.html")
        self.assertEqual(loader.get_contents(origin), "Something completely different")

    def test_get_contents_jinja(self):
        template = create_template(name="jinja2.html", is_active=True, jinja_content="Something else")
        loader = TemplateLoader(self.engine)
        origin = Origin(name="jinja2.html")
        with override_template(template):
            self.assertRaises(TemplateDoesNotExist, loader.get_contents, origin)

    def test_get_contents_active(self):
        template = create_template(name="active.html", content="Something else")
        loader = TemplateLoader(self.engine)
        origin = Origin(name="active.html")
        with override_template(template):
            self.assertEqual(loader.get_contents(origin), "Something else")

    def test_get_contents_not_found(self):
        loader = TemplateLoader(self.engine)
        origin = Origin(name="unknown.html")
        self.assertRaises(TemplateDoesNotExist, loader.get_contents, origin)


class SecretaryEnvironmentTest(TestCase):
    def test_get_secretary_environment(self):
        self.assertIsInstance(
            secretary_environment("django_secretary.utils.templates.JinjaTemplateLoader").loader, JinjaTemplateLoader
        )


class JinjaTemplateLoaderTest(TestCase):
    def setUp(self):
        # Use temporary file storage
        tmp_dir = TempDirectory()
        self.addCleanup(tmp_dir.cleanup)
        storage_patcher = self.settings(MEDIA_ROOT=tmp_dir.path)
        self.addCleanup(storage_patcher.disable)
        storage_patcher.enable()
        self.loader = JinjaTemplateLoader()
        self.environment = Environment()
        self.template = create_template(
            name="example.html", is_active=True, jinja_content="Something completely different"
        )

    def test_get_source(self):
        self.assertEqual(
            self.loader.get_source(self.environment, self.template.uid)[0], "Something completely different"
        )

    def test_get_source_not_uptodate(self):
        self.assertFalse(self.loader.get_source(self.environment, self.template.uid)[2]())

    def test_get_source_inactive(self):
        template = create_template(name="inactive.html", jinja_content="Something else")
        self.assertEqual(self.loader.get_source(self.environment, template.uid)[0], "Something else")

    def test_get_source_django(self):
        template = create_template(name="django.html", content="Something else")
        self.assertRaises(TemplateDoesNotExist, self.loader.get_source, self.environment, template.uid)

    def test_get_source_not_found(self):
        self.assertRaises(
            TemplateDoesNotExist, self.loader.get_source, self.environment, f"{UUID(int=42)}.nonexistent.html"
        )


class TemplateWrapperTest(RenderTestMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.params = {
            "NAME": "django",
            "DIRS": [],
            "APP_DIRS": False,
            "OPTIONS": {"loaders": ["django_secretary.utils.templates.TemplateLoader"]},
        }
        self.jinja_params = {
            "NAME": "django",
            "DIRS": [],
            "APP_DIRS": False,
            "OPTIONS": {
                "loader": "django_secretary.utils.templates.JinjaTemplateLoader",
                "environment": "django_secretary.utils.templates.secretary_environment",
            },
        }
        self.engine = DjangoTemplates(self.params)

    def test_template(self):
        model = create_template(name="example.txt", content="Gazpacho!")
        template = TemplateWrapper(model, [self.engine])

        self.assertEqual(cast(EngineTemplate, template.template).template.name, "example.txt")
        self.assertEqual(cast(EngineTemplate, template.template).template.engine, self.engine.engine)

    def test_template_uid_choose(self):
        model = create_template(name="example.txt", content="Gazpacho!", jinja_content="Gaspacho!")
        cases = [
            (self.engine, model.name),
            (Jinja2(self.jinja_params), model.uid),
        ]
        for engine, uid in cases:
            template = TemplateWrapper(model, [engine])
            self.assertEqual(cast(EngineTemplate, template.template).template.name, uid)

    def test_template_active(self):
        # Test template returns active template.
        model = create_template(name="example.txt", content="Gazpacho!")
        # Create another model with the same name.
        create_template(name="example.txt", is_active=False, content="Other!")
        template = TemplateWrapper(model, [self.engine])

        self.assertEqual(cast(EngineTemplate, template.template).template.name, "example.txt")
        self.assertEqual(cast(EngineTemplate, template.template).template.source, "Gazpacho!")

    def test_template_cached(self):
        # Test template property is cached.
        model = create_template(name="example.txt", content="Gazpacho!")
        template = TemplateWrapper(model, [self.engine])

        self.assertIs(template.template, template.template)

    def test_render(self):
        # Test rendering simple template.
        model = create_template(name="example.txt", content="Hello, I'm {{ name }}.")
        template = TemplateWrapper(model, [self.engine])
        context = {"name": "Kryten"}

        self.assertEqual(template.render(context), "Hello, I'm Kryten.")

        self.assertEqual(self.template_rendered["templates"][0], cast(EngineTemplate, template.template).template)
        self.assertEqual(self.template_rendered["context"][0]["name"], "Kryten")
        self.assertEqual(self.template_rendered["context"][0]["template"], model)

    def test_render_parent(self):
        # Test rendering the template with parent template.
        parent = create_template(name="base.txt", content="Hello, {% block content %}{% endblock %}")
        model = create_template(
            name="example.txt",
            parent=parent,
            content="{% extends template.parent.name %}{% block content %}I'm {{ name }}.{% endblock %}",
        )
        template = TemplateWrapper(model, [self.engine])
        context = {"name": "Kryten"}

        self.assertEqual(template.render(context), "Hello, I'm Kryten.")

        self.assertEqual(self.template_rendered["templates"][0], cast(EngineTemplate, template.template).template)
        self.assertEqual(self.template_rendered["context"][0]["name"], "Kryten")
        self.assertEqual(self.template_rendered["context"][0]["template"], model)
        self.assertEqual(self.template_rendered["templates"][1].name, "base.txt")
        self.assertEqual(self.template_rendered["context"][1]["name"], "Kryten")

    def test_render_invalid(self):
        # Test rendering invalid template.
        model = create_template(name="example.txt", content="Hello, I'm {% invalid %}")
        template = TemplateWrapper(model, [self.engine])

        with self.assertRaisesMessage(TemplateSyntaxError, "Invalid block tag on line 1: 'invalid'."):
            template.render({})


class GetTemplateTest(TestCase):
    engines_call_list = [
        Mock(side_effect=TemplateDoesNotExist("Template not found")),
        Mock(side_effect=TemplateDoesNotExist("Template not found")),
        Mock(side_effect=TemplateDoesNotExist("Template not found")),
        Mock(return_value=None),  # might happen for Jinja2 template engine
        Mock(return_value=sentinel.foo),
        Mock(return_value=sentinel.bar),
        Mock(side_effect=TemplateDoesNotExist("Template not found")),
    ]

    engines_list = [Mock(get_template=x, i=i) for i, x in enumerate(engines_call_list)]

    def test_get_template_ok(self):
        self.assertEqual(_get_template(lambda x: "asdf", self.engines_list), sentinel.foo)  # type: ignore[arg-type]

    def test_get_template_not_found(self):
        self.assertRaises(TemplateDoesNotExist, _get_template, lambda x: "asdf", self.engines_list[:2])

    def test_name_search(self):
        for func in self.engines_list:
            func.reset_mock()
        _get_template(lambda x: str(x.i), self.engines_list)  # type: ignore
        for j, func in enumerate(self.engines_call_list[:3]):
            func.assert_called_once_with(str(j))
