from decimal import Decimal

from django.test import SimpleTestCase

from django_secretary.templatetags.overrides import _DecimalWrapper, floatformat


class DecimalWrapperTest(SimpleTestCase):
    def test_str(self):
        value = "42.123456789012345678901"
        wrapped = _DecimalWrapper(Decimal(value))
        self.assertEqual(str(wrapped), value)

    def test_repr(self):
        value = "42.123456789012345678901"
        wrapped = _DecimalWrapper(Decimal(value))
        self.assertEqual(repr(wrapped), value)


class FloatformatTest(SimpleTestCase):
    def test_valid(self):
        data = (
            # value, arg, result
            (42.123, 3, "42.123"),
            ("42.123", 3, "42.123"),
            ("42.123456789012345678901", 21, "42.123456789012345678901"),
            (Decimal("42.123"), 3, "42.123"),
            (Decimal("42.123456789012345678901"), 21, "42.123456789012345678901"),
        )
        for value, arg, result in data:
            with self.subTest(value=value):
                self.assertEqual(floatformat(value, arg), result)

    def test_invalid(self):
        data = ("", "JUNK")
        for value in data:
            with self.subTest(value=value):
                self.assertEqual(floatformat(value), "")
