from django.template.defaultfilters import date
from django.templatetags.l10n import localize
from django.templatetags.tz import localtime
from django.test import SimpleTestCase

from django_secretary.templatetags.overrides import floatformat
from django_secretary.utils.templates import secretary_environment


class DjangoL10nExtensionTest(SimpleTestCase):
    def test_filters(self):
        env = secretary_environment(
            "django_secretary.utils.templates.JinjaTemplateLoader",
            extensions=["django_secretary.templatetags.django_l10n.DjangoL10nExtension"],
        )

        self.assertEqual(env.filters["date"], date)
        self.assertEqual(env.filters["floatformat"], floatformat)
        self.assertEqual(env.filters["localize"], localize)
        self.assertEqual(env.filters["localtime"], localtime)
