from datetime import date, datetime

import django
from django.test import SimpleTestCase
from pytz import UTC

from django_secretary.templatetags.parsers import parse_date, parse_datetime
from django_secretary.utils.templates import secretary_environment


class ParseDateTest(SimpleTestCase):
    def test_valid(self):
        data = (
            # value, result
            ("2001-02-03", date(2001, 2, 3)),
        )
        for value, result in data:
            with self.subTest(value=value):
                self.assertEqual(parse_date(value), result)

    def test_invalid(self):
        data = ("", "JUNK", 155, "2001-13-03", "2001-02-03 04:05:06")
        for value in data:
            with self.subTest(value=value):
                self.assertIsNone(parse_date(value))  # type: ignore[arg-type]


class ParseDatetimeTest(SimpleTestCase):
    def test_valid(self):
        data = [
            # value, result
            ("2001-02-03 04:05:06", datetime(2001, 2, 3, 4, 5, 6)),
            ("2001-02-03 04:05:06+0000", datetime(2001, 2, 3, 4, 5, 6, tzinfo=UTC)),
            ("2001-02-03T04:05:06", datetime(2001, 2, 3, 4, 5, 6)),
        ]
        if django.VERSION >= (4,):  # pragma: no cover
            data.append(("2001-02-03", datetime(2001, 2, 3, 0)))

        for value, result in data:
            with self.subTest(value=value):
                self.assertEqual(parse_datetime(value), result)

    def test_invalid(self):
        data = ["", "JUNK", 155, "2000-13-03 04:05:06"]
        if django.VERSION < (4,):  # pragma: no cover
            data.append("2001-02-03")
        for value in data:
            with self.subTest(value=value):
                self.assertIsNone(parse_datetime(value))  # type: ignore[arg-type]


class ParsersExtensionTest(SimpleTestCase):
    def test_filters(self):
        env = secretary_environment(
            "django_secretary.utils.templates.JinjaTemplateLoader",
            extensions=["django_secretary.templatetags.parsers.ParsersExtension"],
        )

        self.assertEqual(env.filters["parse_date"], parse_date)
        self.assertEqual(env.filters["parse_datetime"], parse_datetime)
