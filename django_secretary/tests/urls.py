"""URLs for django-secretary tests."""

from django.contrib.admin import site
from django.urls import include, path

urlpatterns = [
    path("", include("django_secretary.urls")),
    path("admin/", site.urls),
]
