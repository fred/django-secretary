from functools import partial
from typing import Any, Dict, Optional, Union, cast

from django.core.files.base import ContentFile
from django.http import HttpResponse
from django.test import Client, TestCase
from django.test.client import store_rendered_templates  # type: ignore[attr-defined]  # Private function
from django.test.signals import template_rendered
from testfixtures import TempDirectory

from django_secretary.models import Asset, Template

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "NAME": "django",
        "OPTIONS": {"loaders": ["django_secretary.utils.templates.TemplateLoader"]},
    },
    {
        "BACKEND": "django.template.backends.jinja2.Jinja2",
        "NAME": "jinja",
        "OPTIONS": {"loader": "django_secretary.utils.templates.JinjaTemplateLoader"},
    },
]


class JsonClient(Client):
    """Test client which sends tries to encode all data as JSON."""

    def get(  # type: ignore[override]
        self,
        path: str,
        data: Any = None,
        follow: bool = False,
        secure: bool = False,
        *,
        content_type: str = "application/json",
        **extra: Any,
    ) -> HttpResponse:
        """Request a response from the server using GET."""
        data = self._encode_json({} if data is None else data, content_type)  # type: ignore[attr-defined]
        body = self._encode_data(data, content_type)  # type: ignore[attr-defined]
        response = cast(HttpResponse, self.generic("GET", path, body, content_type, secure=secure, **extra))
        if follow:  # pragma: no cover
            response = self._handle_redirects(response, data=data, **extra)  # type: ignore[attr-defined]
        return response


def create_template(*, content: Optional[str] = None, jinja_content: Optional[str] = None, **kwargs: Any) -> Template:
    """Create Template model instance with defined content."""
    template: Template = Template.objects.create(**kwargs)
    if content is not None:
        template.content.save("placeholder.txt", ContentFile(content))
    if jinja_content is not None:
        template.jinja_content.save("placeholder.txt", ContentFile(jinja_content))
    template.save()
    return template


def create_asset(*, content: Union[str, bytes], **kwargs: Any) -> Asset:
    """Create Asset model instance with defined content."""
    asset: Asset = Asset.objects.create(**kwargs)
    asset.content.save("placeholder.txt", ContentFile(content))
    asset.save()
    return asset


class RenderTestMixin:
    """Test mixin for tests of template rendering."""

    def setUp(self) -> None:
        # Use temporary file storage
        tmp_dir = TempDirectory()
        cast(TestCase, self).addCleanup(tmp_dir.cleanup)
        media_patcher = cast(TestCase, self).settings(MEDIA_ROOT=tmp_dir.path, MEDIA_URL="/media/")
        cast(TestCase, self).addCleanup(media_patcher.disable)
        media_patcher.enable()

        # Capture template rendering arguments
        self.template_rendered: Dict[str, Any] = {}
        dispatch_uid = "test-template-render-{}".format(id(self))
        template_rendered.connect(
            partial(store_rendered_templates, self.template_rendered), dispatch_uid=dispatch_uid, weak=False
        )
        cast(TestCase, self).addCleanup(template_rendered.disconnect, dispatch_uid=dispatch_uid)
