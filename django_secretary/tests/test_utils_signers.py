import os
import subprocess
from io import BytesIO
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Any, Dict, List, Optional
from unittest.mock import ANY, _Call, call, patch, sentinel

from django.test import TestCase, override_settings
from pyhanko.pdf_utils.font.basic import SimpleFontEngineFactory
from pyhanko.pdf_utils.font.opentype import GlyphAccumulatorFactory
from pyhanko.pdf_utils.images import PdfImage
from pyhanko.pdf_utils.text import TextBoxStyle
from pyhanko.sign.fields import SigFieldSpec
from pyhanko.sign.signers import PdfSignatureMetadata
from pyhanko.stamp import TextStampStyle
from testfixtures import Comparison

from django_secretary.utils.signers import _PYHANKO_FIELD_NAME, SignerNotFound, sign_pdf

HERE = Path(__file__).parent


class SignPdfTest(TestCase):
    def setUp(self):
        # Make test temporary directory and forge it to sign_pdf.
        self.tmp_dir = TemporaryDirectory()
        self.addCleanup(self.tmp_dir.cleanup)
        patcher = patch("django_secretary.utils.signers.TemporaryDirectory", return_value=self.tmp_dir)
        self.addCleanup(patcher.stop)
        patcher.start()

    def create_tmp_file(self, name: str, content: bytes) -> None:
        with open(os.path.join(self.tmp_dir.name, name), "wb") as file:
            file.write(content)

    def test_unknown_signer(self):
        with override_settings(SECRETARY_SIGNERS={}):
            with self.assertRaisesRegex(SignerNotFound, "Signer 'unknown' not found."):
                sign_pdf("unknown", b"")

    ##### Command method #####

    def _test_signed(self, signer: Dict[str, Any], signed_content: bytes, mock_call: _Call) -> None:
        content = b"Gazpacho!"

        with override_settings(SECRETARY_SIGNERS={"captain": signer}):
            with patch("subprocess.run") as run_mock:
                result = sign_pdf("captain", content)

        self.assertEqual(result, signed_content)
        self.assertEqual(run_mock.mock_calls, [mock_call])
        # Check temporary directory was deleted.
        self.assertFalse(os.path.exists(self.tmp_dir.name))

    def test_command(self):
        signed_content = b"Quagaars!"
        self.create_tmp_file("input_signed.pdf", signed_content)

        signer = {"COMMAND": ["hollister", "input.pdf"]}
        mock_call = call(["hollister", "input.pdf"], cwd=ANY, check=True, capture_output=True, env=None)
        self._test_signed(signer, signed_content, mock_call)

    def test_command_output(self):
        signed_content = b"Quagaars!"
        self.create_tmp_file("output.pdf", signed_content)

        signer = {"COMMAND": ["hollister", "input.pdf"], "OUTPUT": "output.pdf"}
        mock_call = call(["hollister", "input.pdf"], cwd=ANY, check=True, capture_output=True, env=None)
        self._test_signed(signer, signed_content, mock_call)

    def test_command_environment(self):
        signed_content = b"Quagaars!"
        self.create_tmp_file("input_signed.pdf", signed_content)

        signer = {"COMMAND": ["hollister", "input.pdf"], "ENVIRONMENT": {"FOO": "BAR"}}
        mock_call = call(["hollister", "input.pdf"], cwd=ANY, check=True, capture_output=True, env={"FOO": "BAR"})
        self._test_signed(signer, signed_content, mock_call)

    def test_command_placeholder(self):
        # Test {output} placeholder in command
        signed_content = b"Quagaars!"
        self.create_tmp_file("input_signed.pdf", signed_content)

        signer = {"COMMAND": ["hollister", "input.pdf", "{output}"]}
        mock_call = call(
            ["hollister", "input.pdf", "input_signed.pdf"], cwd=ANY, check=True, capture_output=True, env=None
        )
        self._test_signed(signer, signed_content, mock_call)

    def test_command_error(self):
        signer = {"COMMAND": ["hollister", "input.pdf"]}

        with override_settings(SECRETARY_SIGNERS={"signer": signer}):
            with patch("subprocess.run") as run_mock:
                run_mock.side_effect = subprocess.CalledProcessError(
                    42, ["my_cmd"], output="Dinner", stderr="Gazpacho!"
                )
                msg = "Command .*my_cmd.* returned non-zero exit status 42. Output: Dinner, error: Gazpacho!"
                with self.assertRaisesRegex(RuntimeError, msg):
                    sign_pdf("signer", b"")

        # Check temporary directory was deleted.
        self.assertFalse(os.path.exists(self.tmp_dir.name))

    ##### Pyhanko method #####

    def _test_pyhanko(
        self,
        signer: Dict[str, Any],
        *,
        signer_call: Optional[_Call] = None,
        append_calls: Optional[List[_Call]] = None,
        stamp_style: Any = None,
    ) -> None:
        content = b"Gazpacho!"
        signed_content = b"Quagaars!"

        with override_settings(SECRETARY_SIGNERS={"captain": signer}):
            with patch("django_secretary.utils.signers.append_signature_field", autospec=True) as append_mock:
                with patch("django_secretary.utils.signers.IncrementalPdfFileWriter", autospec=True) as writer_mock:
                    with patch("django_secretary.utils.signers.SimpleSigner", autospec=True) as signer_mock:
                        signer_mock.load_pkcs12.return_value = sentinel.signer
                        with patch("django_secretary.utils.signers.PdfSigner", autospec=True) as pdf_mock:
                            pdf_mock.return_value.sign_pdf.return_value = BytesIO(signed_content)
                            result = sign_pdf("captain", content)

        self.assertEqual(result, signed_content)

        # Check the writer and the raw PDF
        self.assertEqual(writer_mock.mock_calls, [call(Comparison(BytesIO))])
        self.assertEqual(writer_mock.mock_calls[0][1][0].getvalue(), content)

        self.assertEqual(append_mock.mock_calls, append_calls or [])
        self.assertEqual(signer_mock.mock_calls, [signer_call or call.load_pkcs12("hollister.p12", passphrase=None)])
        stamp_style = stamp_style or TextStampStyle(
            border_width=0, text_box_style=TextBoxStyle(font=Comparison(SimpleFontEngineFactory))
        )
        mock_calls = [
            call(PdfSignatureMetadata(field_name=_PYHANKO_FIELD_NAME), sentinel.signer, stamp_style=stamp_style),
            call().sign_pdf(writer_mock.return_value),
        ]
        self.assertEqual(pdf_mock.mock_calls, mock_calls)

    def test_pyhanko(self):
        signer = {"METHOD": "pyhanko", "CERTIFICATE": "hollister.p12"}

        self._test_pyhanko(signer)

    def test_pyhanko_password(self):
        signer = {"METHOD": "pyhanko", "CERTIFICATE": "hollister.p12", "PASSWORD": "Cook!"}

        self._test_pyhanko(signer, signer_call=call.load_pkcs12("hollister.p12", passphrase=b"Cook!"))

    def test_pyhanko_border_width(self):
        signer = {"METHOD": "pyhanko", "CERTIFICATE": "hollister.p12", "BORDER_WIDTH": 42}
        stamp_style = TextStampStyle(
            border_width=42, text_box_style=TextBoxStyle(font=Comparison(SimpleFontEngineFactory))
        )
        self._test_pyhanko(signer, stamp_style=stamp_style)

    def test_pyhanko_box(self):
        signer = {"METHOD": "pyhanko", "CERTIFICATE": "hollister.p12", "BOX": (100, 200, 300, 400)}
        append_call = call(ANY, sig_field_spec=SigFieldSpec(_PYHANKO_FIELD_NAME, box=(100, 200, 300, 400)))
        self._test_pyhanko(signer, append_calls=[append_call])

    def test_pyhanko_font_file(self):
        signer = {"METHOD": "pyhanko", "CERTIFICATE": "hollister.p12", "FONT_FILE": "Quagaars.odt"}
        font = Comparison(GlyphAccumulatorFactory, font_file="Quagaars.odt", partial=True)
        stamp_style = TextStampStyle(border_width=0, text_box_style=TextBoxStyle(font=font))
        self._test_pyhanko(signer, stamp_style=stamp_style)

    def test_pyhanko_font_size(self):
        signer = {"METHOD": "pyhanko", "CERTIFICATE": "hollister.p12", "FONT_SIZE": 42}
        stamp_style = TextStampStyle(
            border_width=0, text_box_style=TextBoxStyle(font_size=42, font=Comparison(SimpleFontEngineFactory))
        )
        self._test_pyhanko(signer, stamp_style=stamp_style)

    def test_pyhanko_text(self):
        signer = {"METHOD": "pyhanko", "CERTIFICATE": "hollister.p12", "TEXT": "Signed by captain."}

        stamp_style = TextStampStyle(
            stamp_text="Signed by captain.",
            border_width=0,
            text_box_style=TextBoxStyle(font=Comparison(SimpleFontEngineFactory)),
        )
        self._test_pyhanko(signer, stamp_style=stamp_style)

    def test_pyhanko_image(self):
        image_path = HERE / "data" / "minimal.png"
        signer = {"METHOD": "pyhanko", "CERTIFICATE": "hollister.p12", "IMAGE": str(image_path)}

        stamp_style = TextStampStyle(
            background=Comparison(PdfImage),
            border_width=0,
            text_box_style=TextBoxStyle(font=Comparison(SimpleFontEngineFactory)),
        )
        self._test_pyhanko(signer, stamp_style=stamp_style)
