from django.template.defaultfilters import linebreaksbr
from django.test import SimpleTestCase

from django_secretary.templatetags.text_utils import forcewrap
from django_secretary.utils.templates import secretary_environment


class ForcewrapTest(SimpleTestCase):
    def test_plain(self):
        # Test wrap without length argument.
        data = (
            # value, result
            ("", ""),
            ("short", "short"),
            (" \t\na \t\n", "a"),
            ("space separated text", "spaceseparatedtext"),
            ("whitespace\tseparated\ntext", "whitespaceseparatedtext"),
            ("a" * 70, "a" * 70),
            ("a" * 71, "a" * 70 + "\na"),
            ("a" * 155, "\n".join(("a" * 70, "a" * 70, "a" * 15))),
        )
        for value, result in data:
            with self.subTest(value=value):
                self.assertEqual(forcewrap(value), result)

    def test_length(self):
        # Test wrap with length argument.
        data = (
            # value, length, result
            ("Arnold Rimmer", 5, "Arnol\ndRimm\ner"),
            ("Arnold Rimmer", 1, "A\nr\nn\no\nl\nd\nR\ni\nm\nm\ne\nr"),
        )
        for value, length, result in data:
            with self.subTest(value=value):
                self.assertEqual(forcewrap(value, length), result)


class TextUtilsExtensionTest(SimpleTestCase):
    def test_filters(self):
        env = secretary_environment(
            "django_secretary.utils.templates.JinjaTemplateLoader",
            extensions=["django_secretary.templatetags.text_utils.TextUtilsExtension"],
        )

        self.assertEqual(env.filters["forcewrap"], forcewrap)
        self.assertEqual(env.filters["linebreaksbr"], linebreaksbr)
