from django.test import SimpleTestCase

from django_secretary.constants import DnskeyAlgorithm, DnskeyFlag, DnskeyProtocol
from django_secretary.templatetags.fred import (
    parse_dnskey_algorithm,
    parse_dnskey_flags,
    parse_dnskey_protocol,
    state_flag_description,
)
from django_secretary.utils.templates import secretary_environment


class StateFlagDescriptionTest(SimpleTestCase):
    def test_filter(self):
        data = (
            # value, result
            ("linked", "Has relation to other records in the registry"),
            ("expired", "Domain expired"),
            ("unknown", "unknown"),
        )
        for value, result in data:
            with self.subTest(value=value):
                self.assertEqual(state_flag_description(value), result)


class ParseDnskeyAlgorithmTest(SimpleTestCase):
    def test_filter(self):
        data = (
            # value, result
            (2, DnskeyAlgorithm.DH),
            (16, DnskeyAlgorithm.ED448),
            (122, DnskeyAlgorithm.UNASSIGNED_122),
            (251, DnskeyAlgorithm.RESERVED_251),
        )
        for value, result in data:
            with self.subTest(value=value):
                self.assertEqual(parse_dnskey_algorithm(value), result)

    def test_invalid(self):
        data = (None, "JUNK")
        for value in data:
            with self.subTest(value=value):
                self.assertIsNone(parse_dnskey_algorithm(value))  # type: ignore[arg-type]


class ParseDnskeyFlagsTest(SimpleTestCase):
    def test_filter(self):
        data = (
            # value, result
            (1, DnskeyFlag.SECURE_ENTRY_POINT),
            (128, DnskeyFlag.REVOKE),
            (256, DnskeyFlag.ZONE),
            (1024, DnskeyFlag.UNASSIGNED_5),
            (257, DnskeyFlag.ZONE | DnskeyFlag.SECURE_ENTRY_POINT),  # type: ignore[operator]
        )
        for value, result in data:
            with self.subTest(value=value):
                self.assertEqual(parse_dnskey_flags(value), result)

    def test_invalid(self):
        data = (None, "JUNK")
        for value in data:
            with self.subTest(value=value):
                self.assertIsNone(parse_dnskey_flags(value))  # type: ignore[arg-type]


class ParseDnskeyProtocolTest(SimpleTestCase):
    def test_filter(self):
        data = (
            # value, result
            (1, DnskeyProtocol.RESERVED_1),
            (3, DnskeyProtocol.DNSSEC),
            (4, DnskeyProtocol.RESERVED_4),
            (5, DnskeyProtocol.UNASSIGNED_5),
            (255, DnskeyProtocol.RESERVED_255),
        )
        for value, result in data:
            with self.subTest(value=value):
                self.assertEqual(parse_dnskey_protocol(value), result)

    def test_invalid(self):
        data = (None, "JUNK")
        for value in data:
            with self.subTest(value=value):
                self.assertIsNone(parse_dnskey_protocol(value))  # type: ignore[arg-type]


class FredExtensionTest(SimpleTestCase):
    def test_filters(self):
        env = secretary_environment(
            "django_secretary.utils.templates.JinjaTemplateLoader",
            extensions=["django_secretary.templatetags.fred.FredExtension"],
        )

        self.assertEqual(env.filters["parse_dnskey_algorithm"], parse_dnskey_algorithm)
        self.assertEqual(env.filters["parse_dnskey_flags"], parse_dnskey_flags)
        self.assertEqual(env.filters["parse_dnskey_protocol"], parse_dnskey_protocol)
        self.assertEqual(env.filters["state_flag_description"], state_flag_description)
