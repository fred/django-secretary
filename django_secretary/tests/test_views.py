from uuid import UUID

from django.test import TestCase, override_settings
from django.test.signals import setting_changed
from django.urls import reverse
from django.utils.functional import empty

from django_secretary.views import _RENDERERS

from .utils import TEMPLATES, JsonClient, RenderTestMixin, create_template


@override_settings(ROOT_URLCONF="django_secretary.urls", SECRETARY_TEMPLATES=TEMPLATES)
class TestTemplatesViewSet(RenderTestMixin, TestCase):
    client_class = JsonClient

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        def invalidate_renderers_cache(**kwargs):
            _RENDERERS._wrapped = empty

        setting_changed.connect(invalidate_renderers_cache, dispatch_uid="test-template-viewset", weak=False)

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        setting_changed.disconnect(dispatch_uid="test-template-viewset")

    def test_list_empty(self):
        response = self.client.get(reverse("template-list"))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"count": 0, "next": None, "previous": None, "results": []})

    def test_render(self):
        create_template(name="example.html", content="Hello, I'm {{ name }}.", uuid=UUID(int=42))

        context = {"name": "Kryten"}
        with self.settings(SECRETARY_RENDERERS=("django_secretary.renderers.HtmlRenderer",)):
            response = self.client.post(
                reverse("template-render", kwargs={"uuid": UUID(int=42)}),
                data={"context": context},
                content_type="application/json",
            )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response["Content-Type"], "text/html; charset=utf-8")
        self.assertEqual(response.content, b"Hello, I'm Kryten.")

        self.assertEqual(self.template_rendered["templates"][0].name, "example.html")
        self.assertEqual(self.template_rendered["context"][0]["name"], "Kryten")

    def test_render_no_context(self):
        # Test rendering without context.
        create_template(name="example.html", content="Hello, I'm {{ name }}.", uuid=UUID(int=42))

        with self.settings(SECRETARY_RENDERERS=("django_secretary.renderers.HtmlRenderer",)):
            response = self.client.post(reverse("template-render", kwargs={"uuid": UUID(int=42)}))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response["Content-Type"], "text/html; charset=utf-8")
        self.assertEqual(response.content, b"Hello, I'm .")

        self.assertEqual(self.template_rendered["templates"][0].name, "example.html")

    def test_render_active(self):
        create_template(name="example.html", content="Hello, I'm {{ name }}.", is_active=True)

        context = {"name": "Kryten"}
        with self.settings(SECRETARY_RENDERERS=("django_secretary.renderers.HtmlRenderer",)):
            response = self.client.post(
                reverse("active-template-render", kwargs={"name": "example.html"}),
                data={"context": context},
                content_type="application/json",
            )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response["Content-Type"], "text/html; charset=utf-8")
        self.assertEqual(response.content, b"Hello, I'm Kryten.")

        self.assertEqual(self.template_rendered["templates"][0].name, "example.html")
        self.assertEqual(self.template_rendered["context"][0]["name"], "Kryten")

    def test_render_invalid_context(self):
        create_template(name="example.html", content="Hello, I'm {{ name }}.", uuid=UUID(int=42))

        with self.settings(SECRETARY_RENDERERS=("django_secretary.renderers.HtmlRenderer",)):
            response = self.client.post(
                reverse("template-render", kwargs={"uuid": UUID(int=42)}), data={"context": "Not a valid JSON"}
            )

        self.assertEqual(response.status_code, 400)


@override_settings(ROOT_URLCONF="django_secretary.urls")
class TestAssetViewSet(TestCase):
    def test_list_empty(self):
        response = self.client.get(reverse("asset-list"))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"count": 0, "next": None, "previous": None, "results": []})


@override_settings(ROOT_URLCONF="django_secretary.urls")
class TestTemplateCollectionViewSet(TestCase):
    def test_list_empty(self):
        response = self.client.get(reverse("templatecollection-list"))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"count": 0, "next": None, "previous": None, "results": []})
