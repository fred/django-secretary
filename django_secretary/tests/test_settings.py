from typing import Any, Dict, Iterable, Tuple
from unittest.mock import sentinel

from django.core.exceptions import ValidationError
from django.test import SimpleTestCase

from django_secretary.settings import MergeDictSetting, SignersSetting


class MergeDictSettingTest(SimpleTestCase):
    def test_update(self):
        setting = MergeDictSetting(default={"default": sentinel.default, "override": sentinel.original})
        result = {"default": sentinel.default, "override": sentinel.override, "unknown": sentinel.unknown}
        self.assertEqual(setting.transform({"override": sentinel.override, "unknown": sentinel.unknown}), result)


class SignersSettingTest(SimpleTestCase):
    def test_transform(self):
        data: Iterable[Tuple[Dict[str, Any], Dict[str, Any]]] = (
            # value, transformed
            ({}, SignersSetting.SIGNER_DEFAULTS),
            ({"ENVIRONMENT": sentinel.env}, dict(SignersSetting.SIGNER_DEFAULTS, **{"ENVIRONMENT": sentinel.env})),
            ({"OUTPUT": sentinel.output}, dict(SignersSetting.SIGNER_DEFAULTS, **{"OUTPUT": sentinel.output})),
        )
        setting = SignersSetting()
        for value, transformed in data:
            with self.subTest(value=value):
                self.assertEqual(setting.transform({"captain": value}), {"captain": transformed})

    def test_validate_valid(self):
        data: Iterable[Dict[str, Any]] = (
            {},
            {"captain": {"COMMAND": ["sign"]}},
            {"captain": {"METHOD": "command", "COMMAND": ["sign"]}},
            {"captain": {"METHOD": "pyhanko", "CERTIFICATE": ["hollister.p12"]}},
            {"captain": {"METHOD": "pyhanko", "CERTIFICATE": ["hollister.p12"], "BOX": (1, 2, 3, 4)}},
        )
        setting = SignersSetting()
        for value in data:
            with self.subTest(value=value):
                setting.validate(value)

    def test_validate_invalid(self):
        data: Iterable[Tuple[Dict[str, Any], str]] = (
            # value, error
            ({"METHOD": "invalid"}, "Unknown method 'invalid'."),
            # command method
            ({}, "COMMAND option is required for command method."),
            ({"METHOD": "command"}, "COMMAND option is required for command method."),
            ({"COMMAND": "invalid"}, "COMMAND has to be a sequence of strings."),
            ({"COMMAND": [True]}, "COMMAND has to be a sequence of strings."),
            ({"COMMAND": [], "ENVIRONMENT": "invalid"}, "ENVIRONMENT has to be a mapping."),
            ({"COMMAND": [], "OUTPUT": True}, "OUTPUT has to be a string."),
            # pyhanko method
            ({"METHOD": "pyhanko"}, "CERTIFICATE option is required for pyhanko method."),
            ({"METHOD": "pyhanko", "CERTIFICATE": "", "BOX": ()}, "BOX has to be a tuple of 4 integers."),
            ({"METHOD": "pyhanko", "CERTIFICATE": "", "BOX": (1)}, "BOX has to be a tuple of 4 integers."),
            ({"METHOD": "pyhanko", "CERTIFICATE": "", "BOX": (1, 2)}, "BOX has to be a tuple of 4 integers."),
            ({"METHOD": "pyhanko", "CERTIFICATE": "", "BOX": (1, 2, 3)}, "BOX has to be a tuple of 4 integers."),
            ({"METHOD": "pyhanko", "CERTIFICATE": "", "BOX": (1, 2, 3, 4, 5)}, "BOX has to be a tuple of 4 integers."),
            ({"METHOD": "pyhanko", "CERTIFICATE": "", "BOX": [1, 2, 3, 4]}, "BOX has to be a tuple of 4 integers."),
            ({"METHOD": "pyhanko", "CERTIFICATE": "", "IMAGE": True}, "IMAGE has to be a string."),
        )
        setting = SignersSetting()
        for value, error in data:
            with self.subTest(value=value):
                self.assertRaisesRegex(ValidationError, error, setting.validate, {"captain": value})
