from http import HTTPStatus
from typing import cast
from unittest.mock import sentinel

from django.http import HttpResponse
from django.test import TestCase
from jinja2 import UndefinedError

from django_secretary.middleware import JinjaUndefinedMiddleware


class JinjaUndefinedMiddlewareTest(TestCase):
    def test_process_exception(self):
        middleware = JinjaUndefinedMiddleware(sentinel.get_response)
        exception = UndefinedError("Gazpacho!")

        response = cast(HttpResponse, middleware.process_exception(sentinel.request, exception))

        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)
        self.assertEqual(response["Content-Type"], "application/json")
        self.assertJSONEqual(cast(str, response.content), {"error": "Gazpacho!"})

    def test_process_exception_other(self):
        middleware = JinjaUndefinedMiddleware(sentinel.get_response)
        exception = Exception("Gazpacho!")

        self.assertIsNone(middleware.process_exception(sentinel.request, exception))
