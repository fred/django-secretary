from unittest import skipIf

import django
from django.core.exceptions import ImproperlyConfigured
from django.test import TestCase
from testfixtures import TempDirectory

from django_secretary.constants import MimeType
from django_secretary.utils.fetchers import FileFetcher
from django_secretary.utils.templates import override_template

from .utils import create_asset, create_template


class TestFileFetcher(TestCase):
    test_file_content = b"The test file content."

    def setUp(self):
        # Set up temporary directory as a storage
        self.tmp_dir = TempDirectory()
        self.addCleanup(self.tmp_dir.cleanup)
        self.tmp_dir.write("test.css", self.test_file_content)

        # Use the temporary directory also for MEDIA_ROOT.
        storage_patcher = self.settings(MEDIA_ROOT=self.tmp_dir.path, MEDIA_URL="/media/")
        self.addCleanup(storage_patcher.disable)
        storage_patcher.enable()

    def test_init(self):
        fetcher = FileFetcher("http://example.net/")

        self.assertEqual(fetcher.media_url, "http://example.net/media/")

    @skipIf(django.VERSION >= (3, 1), "Only affects Django <=3.0")
    def test_init_no_url(self):  # pragma: no cover
        with self.assertRaisesMessage(ImproperlyConfigured, "Django-secretary fetcher requires non-empty MEDIA_URL."):
            with self.settings(MEDIA_ROOT=self.tmp_dir.path, MEDIA_URL=""):
                FileFetcher("http://example.net/")

    def test_init_no_root(self):
        with self.assertRaisesMessage(ImproperlyConfigured, "Django-secretary fetcher requires non-empty MEDIA_ROOT."):
            with self.settings(MEDIA_ROOT="", MEDIA_URL="/media/"):
                FileFetcher("http://example.net/")

    def test_fetch_url(self):
        # Fetch assets by its URL.
        fetcher = FileFetcher("http://example.net/")

        data = fetcher.fetch("http://example.net/media/test.css")
        self.addCleanup(data["file_obj"].close)

        self.assertCountEqual(data.keys(), ("file_obj", "mime_type", "redirected_url"))
        self.assertEqual(data["file_obj"].read(), self.test_file_content)
        self.assertEqual(data["mime_type"], "text/css")
        self.assertEqual(data["redirected_url"], "http://example.net/media/test.css")

    def test_fetch_name_global(self):
        # Fetch global asset by its name.
        create_asset(name="example.css", mime_type=MimeType.CSS, content=self.test_file_content)
        fetcher = FileFetcher("http://example.net/")

        data = fetcher.fetch("http://example.net/media/example.css")
        self.addCleanup(data["file_obj"].close)

        self.assertCountEqual(data.keys(), ("file_obj", "mime_type", "redirected_url"))
        self.assertEqual(data["file_obj"].read(), self.test_file_content)
        self.assertEqual(data["mime_type"], "text/css")
        self.assertEqual(data["redirected_url"], "http://example.net/media/example.css")

    def test_fetch_name_template(self):
        # Fetch a template asset by its name.
        template = create_template(content="placeholder")
        create_asset(template=template, name="example.css", mime_type=MimeType.CSS, content=self.test_file_content)
        fetcher = FileFetcher("http://example.net/")

        with override_template(template):
            data = fetcher.fetch("http://example.net/media/example.css")
            self.addCleanup(data["file_obj"].close)

        self.assertCountEqual(data.keys(), ("file_obj", "mime_type", "redirected_url"))
        self.assertEqual(data["file_obj"].read(), self.test_file_content)
        self.assertEqual(data["mime_type"], "text/css")
        self.assertEqual(data["redirected_url"], "http://example.net/media/example.css")

    def test_fetch_name_template_parent(self):
        # Fetch a template parent asset by its name.
        parent = create_template(content="parent placeholder")
        create_asset(template=parent, name="example.css", mime_type=MimeType.CSS, content=self.test_file_content)
        template = create_template(content="placeholder", parent=parent)
        fetcher = FileFetcher("http://example.net/")

        with override_template(template):
            data = fetcher.fetch("http://example.net/media/example.css")
            self.addCleanup(data["file_obj"].close)

        self.assertCountEqual(data.keys(), ("file_obj", "mime_type", "redirected_url"))
        self.assertEqual(data["file_obj"].read(), self.test_file_content)
        self.assertEqual(data["mime_type"], "text/css")
        self.assertEqual(data["redirected_url"], "http://example.net/media/example.css")

    def test_fetch_name_priority_parent(self):
        # Ensure a template asset has priority over the parent one.
        parent = create_template(content="parent placeholder")
        create_asset(template=parent, name="example.css", mime_type=MimeType.CSS, content="parent asset")
        template = create_template(content="placeholder", parent=parent)
        create_asset(template=template, name="example.css", mime_type=MimeType.CSS, content=self.test_file_content)
        fetcher = FileFetcher("http://example.net/")

        with override_template(template):
            data = fetcher.fetch("http://example.net/media/example.css")
            self.addCleanup(data["file_obj"].close)

        self.assertCountEqual(data.keys(), ("file_obj", "mime_type", "redirected_url"))
        self.assertEqual(data["file_obj"].read(), self.test_file_content)
        self.assertEqual(data["mime_type"], "text/css")
        self.assertEqual(data["redirected_url"], "http://example.net/media/example.css")

    def test_fetch_name_priority_global(self):
        # Ensure a template asset has priority over the global one.
        create_asset(name="example.css", mime_type=MimeType.CSS, content="global asset")
        template = create_template(content="placeholder")
        create_asset(template=template, name="example.css", mime_type=MimeType.CSS, content=self.test_file_content)
        fetcher = FileFetcher("http://example.net/")

        with override_template(template):
            data = fetcher.fetch("http://example.net/media/example.css")
            self.addCleanup(data["file_obj"].close)

        self.assertCountEqual(data.keys(), ("file_obj", "mime_type", "redirected_url"))
        self.assertEqual(data["file_obj"].read(), self.test_file_content)
        self.assertEqual(data["mime_type"], "text/css")
        self.assertEqual(data["redirected_url"], "http://example.net/media/example.css")

    def test_fetch_name_unlinked(self):
        # Test unlinked asset is not fetched.
        other_template = create_template(content="other")
        create_asset(
            template=other_template, name="example.css", mime_type=MimeType.CSS, content=self.test_file_content
        )
        template = create_template(content="placeholder")
        fetcher = FileFetcher("http://example.net/")

        url = "http://example.net/media/example.css"
        filename = self.tmp_dir.getpath("example.css")
        with override_template(template):
            with self.assertRaisesMessage(ValueError, "Media file {} not found at {}.".format(url, filename)):
                fetcher.fetch(url)

    def test_fetch_not_found(self):
        url = "http://example.net/media/unknown.txt"
        filename = self.tmp_dir.getpath("unknown.txt")
        with self.assertRaisesMessage(ValueError, "Media file {} not found at {}.".format(url, filename)):
            fetcher = FileFetcher("http://example.net/")
            fetcher.fetch(url)

    def test_fetch_external(self):
        with self.assertRaisesMessage(ValueError, "URI http://example.net/other.txt is out of base URL."):
            fetcher = FileFetcher("http://example.net/")
            fetcher.fetch("http://example.net/other.txt")

    def test_fetch_url_multi_slash(self):
        fetcher = FileFetcher("http://example.net/")
        cases = [
            "http://example.net/media//test.css",
            "http://example.net//media/test.css",
        ]

        for url in cases:
            data = fetcher.fetch(url)
            self.addCleanup(data["file_obj"].close)

            self.assertCountEqual(data.keys(), ("file_obj", "mime_type", "redirected_url"))
            self.assertEqual(data["file_obj"].read(), self.test_file_content)
            self.assertEqual(data["mime_type"], "text/css")
            self.assertEqual(data["redirected_url"], "http://example.net/media/test.css")
