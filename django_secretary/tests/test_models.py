from unittest.mock import sentinel
from uuid import UUID

import django
from django.forms import Media
from django.test import SimpleTestCase, TestCase
from testfixtures import TempDirectory

from django_secretary.constants import MimeType
from django_secretary.models import Asset, Tag, Template, get_template_from_uid, make_file_name, unpack_uid

from .utils import create_asset


class TestMakeFileName(SimpleTestCase):
    def test_template(self):
        template = Template(name="example.html")
        self.assertEqual(make_file_name(template, sentinel.original), "example-{}.html".format(template.uuid))

    def test_asset(self):
        asset = Asset(name="example.css")
        self.assertEqual(make_file_name(asset, sentinel.original), "example-{}.css".format(asset.uuid))

    def test_prefix(self):
        template = Template(name="example.html")
        self.assertEqual(
            make_file_name(template, sentinel.original, "pre/fix"), "pre/fix/example-{}.html".format(template.uuid)
        )


class TestTemplate(TestCase):
    def setUp(self):
        # Use temporary file storage
        tmp_dir = TempDirectory()
        self.addCleanup(tmp_dir.cleanup)
        storage_patcher = self.settings(MEDIA_ROOT=tmp_dir.path)
        self.addCleanup(storage_patcher.disable)
        storage_patcher.enable()

    def test_str(self):
        template = Template(name="example.html", uuid=UUID(int=42))
        self.assertEqual(str(template), "example.html (00000000-0000-0000-0000-00000000002a.example.html)")

    def test_media_empty(self):
        template = Template.objects.create()
        self.assertIsInstance(template.media, Media)
        self.assertEqual(str(template.media), "")

    def test_media(self):
        template = Template.objects.create()
        css = create_asset(template=template, name="example.css", mime_type=MimeType.CSS, content="/* Placeholder */")
        css_url = "/media/assets/example-{}.css".format(css.uuid)
        with self.settings(MEDIA_URL="/media/"):
            self.assertIsInstance(template.media, Media)
            # Django 4.1 dropped the type attribute.
            if django.VERSION >= (4, 1):  # pragma: no cover
                type_css = ""
            else:  # pragma: no cover
                type_css = ' type="text/css"'
            html = '<link href="{}" media="all" rel="stylesheet"{}>'.format(css_url, type_css)
            self.assertHTMLEqual(str(template.media), html)

    def test_media_no_images(self):
        # Test that media doesn't contain images.
        template = Template.objects.create()
        create_asset(template=template, name="example.svg", mime_type=MimeType.SVG, content="/* Placeholder */")
        self.assertIsInstance(template.media, Media)
        self.assertEqual(str(template.media), "")

    def test_images_empty(self):
        template = Template.objects.create()
        self.assertEqual(template.images, {})

    def test_images(self):
        template = Template.objects.create()
        image = create_asset(template=template, name="example.svg", mime_type=MimeType.SVG, content="/* Placeholder */")
        self.assertEqual(template.images, {"example.svg": image.content})

    def test_uid(self):
        template = Template(name="example.html", uuid=UUID(int=42))
        self.assertEqual(template.uid, f"{UUID(int=42)}.example.html")


class TagTest(SimpleTestCase):
    def test_str(self):
        tag = Tag(name="example")
        self.assertEqual(str(tag), "example")


class UnpackUidTest(TestCase):
    def test_uid(self):
        template = Template(name="examplehtml", uuid=UUID(int=42))
        self.assertEqual(unpack_uid(template.uid), (template.uuid, template.name))

    def test_uid_splitter_in_name(self):
        template = Template(name="examp.le.html", uuid=UUID(int=42))
        self.assertEqual(unpack_uid(template.uid), (template.uuid, template.name))


class GetTemplateFromUidTest(TestCase):
    def test_correct(self):
        template = Template(name="examplehtml", uuid=UUID(int=42))
        template.save()
        self.assertEqual(get_template_from_uid(template.uid), template)

    def test_invalid(self):
        template = Template(name="examplehtml", uuid=UUID(int=42))
        template.save()
        self.assertRaisesRegex(
            Template.DoesNotExist,
            "Template matching query does not exist.",
            get_template_from_uid,
            template.uid + "foobar",
        )
