from django.test import SimpleTestCase
from django.utils import translation

from django_secretary.utils.templates import secretary_environment


class GetCurrentLanguageTest(SimpleTestCase):
    def test(self):
        env = secretary_environment(
            "django_secretary.utils.templates.JinjaTemplateLoader",
            extensions=["django_secretary.templatetags.django_i18n.GetCurrentLanguage"],
        )
        template = env.from_string("{% get_current_language %}")

        with translation.override("welsh"):
            self.assertEqual(template.render({}), "welsh")


class LanguageTest(SimpleTestCase):
    def test(self):
        env = secretary_environment(
            "django_secretary.utils.templates.JinjaTemplateLoader",
            extensions=[
                "django_secretary.templatetags.django_i18n.GetCurrentLanguage",
                "django_secretary.templatetags.django_i18n.Language",
            ],
        )
        template = env.from_string("{% language 'welsh' %}{% get_current_language %}{% endlanguage %}")

        with translation.override("en"):
            self.assertEqual(template.render({}), "welsh")


class DjangoI18nExtensionTest(SimpleTestCase):
    def test(self):
        env = secretary_environment(
            "django_secretary.utils.templates.JinjaTemplateLoader",
            extensions=["django_secretary.templatetags.django_i18n.DjangoI18nExtension"],
        )
        data = (
            ("{% get_current_language %}", "welsh"),
            ("{% get_current_language as lang %} --- {{ lang }}", " --- welsh"),
            ("{% language 'en' %}{% get_current_language %}{% endlanguage %}", "en"),
        )

        for template_str, output in data:
            with self.subTest(template_str=template_str):
                with translation.override("welsh"):
                    template = env.from_string(template_str)
                    self.assertEqual(template.render({}), output)
