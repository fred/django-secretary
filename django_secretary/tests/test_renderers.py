import types
from typing import Dict, List, Optional, Tuple, cast
from unittest.mock import call, patch, sentinel
from uuid import UUID

from django.core.exceptions import ImproperlyConfigured
from django.template.backends.django import DjangoTemplates, Template as DjangoTemplate
from django.template.loaders.cached import Loader
from django.test import RequestFactory, TestCase, override_settings
from rest_framework.renderers import BaseRenderer
from rest_framework.request import Request
from rest_framework.response import Response
from testfixtures import Comparison, LogCapture

from django_secretary.renderers import PdfRenderer, SignedPdfRenderer, TemplateRendererMixin, TextRenderer
from django_secretary.utils.signers import SignerNotFound
from django_secretary.utils.templates import TemplateLoader, TemplateWrapper
from django_secretary.views import TemplateViewSet

from .utils import TEMPLATES, RenderTestMixin, create_template


class TestTemplateRenderer(TemplateRendererMixin, BaseRenderer):
    """Simple renderer for tests of TemplateRendererMixin."""


class PreferredTestTemplateRenderer(TestTemplateRenderer):
    """Simple renderer with template engine preferences."""

    preferred_engines = ("prefer", "also-prefer")


@override_settings(SECRETARY_TEMPLATES=TEMPLATES)
class TemplateRendererMixinTest(RenderTestMixin, TestCase):
    def setUp(self):
        super().setUp()

        self.uuid = UUID(int=42)
        self.model = create_template(name="example.txt", content="Hello, I'm {{ name }}.", uuid=self.uuid)
        request = Request(RequestFactory().get("/dummy/"))
        self.view = TemplateViewSet(request=request, kwargs={"uuid": str(self.uuid)})
        self.renderer = TestTemplateRenderer()

    def test_get_engine(self):
        engine = cast(DjangoTemplates, self.renderer._get_engines()[0])

        # Check secretary engine is returned.
        self.assertEqual(engine.name, "django")
        self.assertIsInstance(engine.engine.template_loaders[0], TemplateLoader)

    def test_get_engine_none_defined(self):
        # Test error is returned when no template engine is defined.
        with override_settings(SECRETARY_TEMPLATES=[]):
            with self.assertRaisesRegex(ImproperlyConfigured, "No secretary template engine is defined."):
                self.renderer._get_engines()

    def test_get_engine_preferred(self):
        # Test preferred backend is returned when available.
        renderer = PreferredTestTemplateRenderer()
        cases: List[Tuple[List[Dict], str]] = [
            ([], "django"),
            ([{"BACKEND": "django.template.backends.django.DjangoTemplates", "NAME": "prefer"}], "prefer"),
            ([{"BACKEND": "django.template.backends.django.DjangoTemplates", "NAME": "also-prefer"}], "also-prefer"),
            (
                [
                    {"BACKEND": "django.template.backends.django.DjangoTemplates", "NAME": "prefer"},
                    {"BACKEND": "django.template.backends.django.DjangoTemplates", "NAME": "also-prefer"},
                ],
                "prefer",
            ),
            (
                [
                    {"BACKEND": "django.template.backends.django.DjangoTemplates", "NAME": "also-prefer"},
                    {"BACKEND": "django.template.backends.django.DjangoTemplates", "NAME": "prefer"},
                ],
                "also-prefer",
            ),
            (
                [
                    {"BACKEND": "django.template.backends.django.DjangoTemplates", "NAME": "notprefer"},
                    {"BACKEND": "django.template.backends.django.DjangoTemplates", "NAME": "prefer"},
                    {"BACKEND": "django.template.backends.django.DjangoTemplates", "NAME": "also-prefer"},
                ],
                "prefer",
            ),
            (
                [
                    {"BACKEND": "django.template.backends.django.DjangoTemplates", "NAME": "also-prefer"},
                    {"BACKEND": "django.template.backends.django.DjangoTemplates", "NAME": "notprefer"},
                    {"BACKEND": "django.template.backends.django.DjangoTemplates", "NAME": "prefer"},
                ],
                "also-prefer",
            ),
        ]
        for added, expected in cases:
            with self.subTest(added=added):
                templates = TEMPLATES + added

                with override_settings(SECRETARY_TEMPLATES=templates):
                    engine = cast(DjangoTemplates, renderer._get_engines()[0])

                # Check the correct secretary backend is returned.
                self.assertEqual(engine.name, expected)

    def test_get_engine_preferred_unavailable(self):
        # Test preferred backend is returned when unavailable.
        renderer = PreferredTestTemplateRenderer()
        engine = cast(DjangoTemplates, renderer._get_engines()[0])

        # Check secretary backend is returned.
        self.assertEqual(engine.name, "django")
        self.assertIsInstance(engine.engine.template_loaders[0], TemplateLoader)

    def test_get_template(self):
        renderer_context = {"response": Response(), "view": self.view}

        template = cast(TemplateWrapper, self.renderer._get_template(renderer_context))

        self.assertIsInstance(template, TemplateWrapper)
        self.assertEqual(cast(DjangoTemplate, template.template).template.name, "example.txt")
        self.assertEqual(template.model, self.model)
        # Check secretary engine is used.
        self.assertIsInstance(
            template.engines[0].engine.template_loaders[0],  # type: ignore[attr-defined]
            TemplateLoader,
        )

    def test_get_template_error(self):
        renderer_context = {"response": Response(status=500, exception=True), "view": self.view}

        template = cast(DjangoTemplate, self.renderer._get_template(renderer_context))

        self.assertIsInstance(template, DjangoTemplate)
        self.assertEqual(template.template.name, "django_secretary/render_error.txt")
        # Check standard Django engine is used.
        self.assertIsInstance(template.backend.engine.template_loaders[0], Loader)

    def test_get_template_context(self):
        renderer_context = {"response": Response(), "view": self.view}

        self.assertEqual(self.renderer._get_template_context(sentinel.data, renderer_context), sentinel.data)

    def test_get_template_context_error(self):
        renderer_context = {"response": Response(status=500, exception=True), "view": self.view}

        self.assertEqual(
            self.renderer._get_template_context(sentinel.data, renderer_context), {"errors": sentinel.data}
        )


@override_settings(SECRETARY_TEMPLATES=TEMPLATES)
class TestTextRenderer(RenderTestMixin, TestCase):
    def test_render(self):
        uuid = UUID(int=42)
        create_template(name="example.txt", content="Hello, I'm {{ name }}.", uuid=uuid)
        data = {"name": "Kryten"}
        request = Request(RequestFactory().get("/dummy/"))
        view = TemplateViewSet(request=request, kwargs={"uuid": str(uuid)})
        renderer_context = {
            "response": Response(),
            "view": view,
        }
        renderer = TextRenderer()

        result = renderer.render(data, renderer_context=renderer_context)

        self.assertEqual(result, "Hello, I'm Kryten.")
        self.assertEqual(self.template_rendered["templates"][0].name, "example.txt")
        self.assertEqual(self.template_rendered["context"][0]["name"], "Kryten")

    def test_render_error(self):
        renderer_context = {
            "response": Response(status=500, exception=True),
        }
        renderer = TextRenderer()
        data = {"field_42": ["Invalid value"]}

        result = renderer.render(data, renderer_context=renderer_context)

        self.assertEqual(result.strip(), str(data))
        self.assertEqual(self.template_rendered["templates"][0].name, "django_secretary/render_error.txt")
        self.assertEqual(self.template_rendered["context"][0]["errors"], data)


@override_settings(SECRETARY_TEMPLATES=TEMPLATES)
class TestPdfRenderer(RenderTestMixin, TestCase):
    def test_render(self):
        uuid = UUID(int=42)
        create_template(name="example.html", content="Hello, I'm {{ name }}.", uuid=uuid)
        data = {"name": "Kryten"}
        request = Request(RequestFactory().get("/dummy/"))
        view = TemplateViewSet(request=request, kwargs={"uuid": str(uuid)})
        renderer_context = {
            "request": request,
            "response": Response(),
            "view": view,
        }
        renderer = PdfRenderer()

        with patch("django_secretary.renderers.weasyprint.HTML") as weasy_mock:
            weasy_mock.return_value.write_pdf.return_value = sentinel.pdf
            result = renderer.render(data, renderer_context=renderer_context)

        self.assertEqual(result, sentinel.pdf)
        self.assertEqual(self.template_rendered["templates"][0].name, "example.html")
        self.assertEqual(self.template_rendered["context"][0]["name"], "Kryten")
        calls = [
            call(
                string="Hello, I'm Kryten.",
                url_fetcher=Comparison(types.MethodType),
                base_url="http://testserver/dummy/",
            ),
            call().write_pdf(),
        ]
        self.assertEqual(weasy_mock.mock_calls, calls)

    def test_render_error(self):
        renderer_context = {
            "request": Request(RequestFactory().get("/dummy/")),
            "response": Response(status=500, exception=True),
        }
        renderer = PdfRenderer()
        data = {"field_42": ["Invalid value"]}

        with patch("django_secretary.renderers.weasyprint.HTML") as weasy_mock:
            weasy_mock.return_value.write_pdf.return_value = sentinel.pdf
            result = renderer.render(data, renderer_context=renderer_context)

        self.assertEqual(result, sentinel.pdf)
        self.assertEqual(self.template_rendered["templates"][0].name, "django_secretary/render_error.txt")
        self.assertEqual(self.template_rendered["context"][0]["errors"], data)


@override_settings(SECRETARY_TEMPLATES=TEMPLATES)
class SignedPdfRendererTest(RenderTestMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.log_handler = LogCapture("django_secretary", propagate=False)

    def tearDown(self):
        self.log_handler.uninstall()
        super().tearDown()

    def test_signed(self):
        uuid = UUID(int=42)
        create_template(name="example.html", content="Hello, I'm {{ name }}.", uuid=uuid, signer="captain")
        request = Request(RequestFactory().get("/dummy/"))
        view = TemplateViewSet(request=request, kwargs={"uuid": str(uuid)})
        renderer_context = {
            "request": request,
            "response": Response(),
            "view": view,
        }
        renderer = SignedPdfRenderer()

        with patch("django_secretary.renderers.weasyprint.HTML") as weasy_mock:
            weasy_mock.return_value.write_pdf.return_value = sentinel.pdf
            with patch("django_secretary.renderers.sign_pdf", autospec=True, return_value=sentinel.signed) as sign_mock:
                result = renderer.render({"name": "Kryten"}, renderer_context=renderer_context)

        self.assertEqual(result, sentinel.signed)
        self.assertEqual(sign_mock.mock_calls, [call("captain", sentinel.pdf)])

    def _test_render_unsigned(self, signer: Optional[str]) -> None:
        uuid = UUID(int=42)
        create_template(name="example.html", content="Hello, I'm {{ name }}.", uuid=uuid, signer=signer)
        request = Request(RequestFactory().get("/dummy/"))
        view = TemplateViewSet(request=request, kwargs={"uuid": str(uuid)})
        renderer_context = {
            "request": request,
            "response": Response(),
            "view": view,
        }
        renderer = SignedPdfRenderer()

        with patch("django_secretary.renderers.weasyprint.HTML") as weasy_mock:
            weasy_mock.return_value.write_pdf.return_value = sentinel.pdf
            with patch("django_secretary.renderers.sign_pdf", autospec=True, side_effect=SignerNotFound):
                result = renderer.render({"name": "Kryten"}, renderer_context=renderer_context)

        self.assertEqual(result, sentinel.pdf)

    def test_render_unsigned_no_signer(self):
        with override_settings(SECRETARY_SIGNERS={}):
            self._test_render_unsigned(None)

    def test_render_unsigned_unknown_signer(self):
        with override_settings(SECRETARY_SIGNERS={}):
            self._test_render_unsigned("unknown")

    def test_render_error(self):
        renderer_context = {
            "request": Request(RequestFactory().get("/dummy/")),
            "response": Response(status=500, exception=True),
        }
        renderer = SignedPdfRenderer()

        with patch("subprocess.run") as run_mock:
            with patch("django_secretary.renderers.weasyprint.HTML") as weasy_mock:
                weasy_mock.return_value.write_pdf.return_value = sentinel.pdf
                result = renderer.render({"name": "Kryten"}, renderer_context=renderer_context)

        self.assertEqual(result, sentinel.pdf)
        self.assertEqual(run_mock.mock_calls, [])
