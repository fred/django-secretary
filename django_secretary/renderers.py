"""Renderers for django-secretary."""

import logging
from typing import TYPE_CHECKING, Any, ClassVar, Dict, List, Optional, Tuple, cast

import weasyprint
from django.core.exceptions import ImproperlyConfigured
from django.template import loader
from rest_framework.renderers import BaseRenderer

from .protocols import Engine, Template
from .utils.fetchers import FileFetcher
from .utils.signers import SignerNotFound, sign_pdf

if TYPE_CHECKING:
    from .models import Template as TemplateModel

_LOGGER = logging.getLogger(__name__)


class TemplateRendererMixin:
    """A mixin with utilities for template renderers.

    Class attributes:
        preferred_engine: Name of the preferred template engine.
        exception_template: A template for rendering an error.
    """

    preferred_engines: ClassVar[Tuple[str, ...]] = ()
    exception_template: str = "django_secretary/render_error.txt"

    def _get_engines(self) -> List[Engine]:
        """Return a list of template engines to be used.

        Returns the engines, sorted in a way, such that:
            the preferred engines are first, and the rest follows (in case the preferred engines fail),
            the preferred engines are submitted in the order they appear in in the config,
            the non-preferred engines are then also submitted in the order they appear in in the config.
        """
        from .utils.templates import TEMPLATE_ENGINES

        if not TEMPLATE_ENGINES.all():
            raise ImproperlyConfigured("No secretary template engine is defined.")

        # re-order the engines, such that the preferred engines are first.
        return cast(
            List[Engine],
            [backend for backend in TEMPLATE_ENGINES.all() if backend.name in self.preferred_engines]
            + [backend for backend in TEMPLATE_ENGINES.all() if backend.name not in self.preferred_engines],
        )

    def _get_template(self, renderer_context: Dict[str, Any]) -> Template:
        """Return template to be rendered."""
        from .utils.templates import TemplateWrapper

        if renderer_context["response"].exception:
            return cast(Template, loader.get_template(self.exception_template))

        model = cast("TemplateModel", renderer_context["view"].get_object())
        return TemplateWrapper(model, self._get_engines())

    def _get_template_context(self, data: Any, renderer_context: Dict[str, Any]) -> Any:
        """Return a context data for the template."""
        if renderer_context["response"].exception:
            return {"errors": data}
        return data


class TextRenderer(TemplateRendererMixin, BaseRenderer):
    """Renderer which renders secretary template as a plain text.

    Attributes:
        exception_template: A template for rendering an error.
    """

    media_type = "text/plain"
    format = "txt"

    preferred_engines = ("text", "text_django", "text_jinja")

    def render(
        self,
        data: Any,
        media_type: Optional[str] = None,
        renderer_context: Optional[Dict[str, Any]] = None,
    ) -> str:
        """Render secretary template as a plain text."""
        renderer_context = renderer_context or {}
        return self._get_template(renderer_context).render(self._get_template_context(data, renderer_context))


class HtmlRenderer(TextRenderer):
    """Renderer which renders secretary template as HTML."""

    media_type = "text/html"
    format = "html"

    preferred_engines = ("html", "html_django", "html_jinja")
    exception_template = "django_secretary/render_error.html"


class PdfRenderer(TemplateRendererMixin, BaseRenderer):
    """Renderer which renders secretary template as PDF.

    This rendered shouldn't be used with SignedPdfRenderer.
    """

    media_type = "application/pdf"
    format = "pdf"

    preferred_engines = ("html", "html_django", "html_jinja")

    def render(
        self,
        data: Any,
        media_type: Optional[str] = None,
        renderer_context: Optional[Dict[str, Any]] = None,
    ) -> bytes:
        """Render secretary template as PDF."""
        renderer_context = renderer_context or {}
        rendered_template = self._get_template(renderer_context).render(
            self._get_template_context(data, renderer_context)
        )
        request = renderer_context["request"]
        fetcher = FileFetcher(request.build_absolute_uri())
        pdf_renderer = weasyprint.HTML(
            string=rendered_template, url_fetcher=fetcher.fetch, base_url=request.build_absolute_uri()
        )
        return cast(bytes, pdf_renderer.write_pdf())


class SignedPdfRenderer(PdfRenderer):
    """Renderer which renders secretary template as PDF and signs it.

    This rendered shouldn't be used with PdfRenderer.
    """

    def render(
        self,
        data: Any,
        media_type: Optional[str] = None,
        renderer_context: Optional[Dict[str, Any]] = None,
    ) -> bytes:
        """Render secretary template as PDF and sign it."""
        raw_pdf = super().render(data, media_type, renderer_context)
        template = self._get_template(renderer_context or {})
        signer = getattr(getattr(template, "model", None), "signer", None)

        if not signer:
            return raw_pdf

        try:
            return sign_pdf(signer, raw_pdf)
        except SignerNotFound as error:
            _LOGGER.warning("%s", error)
            return raw_pdf
