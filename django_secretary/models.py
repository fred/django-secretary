"""Models for django-secretary."""

import os.path
import uuid
from functools import partial
from typing import Dict, Tuple, Union

from django.db import models
from django.db.models.fields.files import FieldFile
from django.forms import Media
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _

from .constants import IMAGE_MIME_TYPES, MimeType


def make_file_name(instance: Union["Template", "Asset"], filename: str, prefix: str = "") -> str:
    """Return unique file name for a model file.

    Arguments:
        instance: An instance where the file is uploaded.
        filename: Original filename.
        prefix: Prefix of the location where the file is stored to.
    """
    base, extension = os.path.splitext(instance.name)
    return os.path.join(prefix, "{}-{}{}".format(base, instance.uuid, extension))


def unpack_uid(uid: str) -> Tuple[uuid.UUID, str]:
    """Unpack `uid` to `uuid` and `name`."""
    parsed_uuid, parsed_name = uid.split(".", 1)
    return uuid.UUID(parsed_uuid), parsed_name


class Template(models.Model):
    """Represent a single document template.

    Attributes:
        name: Name of the template.
        is_active: Whether the template is active.
        uuid: Unique ID of the template.
        content: The template content.
        parent: The parent template if any.
        signer: A signer used to sign the rendered template.
    """

    name = models.TextField()
    is_active = models.BooleanField(default=False)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    create_datetime = models.DateTimeField(auto_now_add=True)
    content = models.FileField(upload_to=partial(make_file_name, prefix="templates"), null=True, blank=True)
    jinja_content = models.FileField(upload_to=partial(make_file_name, prefix="jinja_templates"), null=True, blank=True)
    parent = models.ForeignKey("django_secretary.Template", blank=True, null=True, on_delete=models.PROTECT)
    signer = models.TextField(blank=True, null=True)

    # Nontrivial annotations
    asset_set: models.Manager

    class Meta:
        constraints = [
            models.UniqueConstraint(
                name="django_secretary_active_template", fields=["name"], condition=models.Q(is_active=True)
            ),
            models.CheckConstraint(
                name="both_contents_not_null",
                check=models.Q(content__isnull=False) | models.Q(jinja_content__isnull=False),
            ),
        ]

    def __str__(self) -> str:
        return "{} ({})".format(self.name, self.uid)

    @cached_property
    def media(self) -> Media:
        """Return a media object with related CSS files."""
        return Media(css={"all": [a.content.url for a in self.asset_set.filter(mime_type=MimeType.CSS)]})

    @cached_property
    def images(self) -> Dict[str, FieldFile]:
        """Return a mapping or name to objects for related images."""
        return {a.name: a.content for a in self.asset_set.filter(mime_type__in=IMAGE_MIME_TYPES)}

    @property
    def uid(self) -> str:
        """Get the `uid` of a model."""
        return f"{self.uuid}.{self.name}"


def get_template_from_uid(uid: str) -> Template:
    """Unpack the uid and get the corresponding template."""
    uuid, name = unpack_uid(uid)
    template = Template.objects.get(uuid=uuid, name=name)
    return template


MIME_TYPE_CHOICES = (
    (MimeType.CSS.value, _("CSS")),
    (MimeType.SVG.value, _("SVG")),
    (MimeType.PNG.value, _("PNG")),
)


class Asset(models.Model):
    """Represent a template asset.

    Attributes:
        template: Linked template. If empty, the asset is available to all templates.
        name: Name of the asset.
        uuid: Unique ID of the asset.
        content: The asset content.
        mime_type: The asset mime type.
    """

    template = models.ForeignKey(Template, blank=True, null=True, on_delete=models.PROTECT)
    name = models.TextField()
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    content = models.FileField(upload_to=partial(make_file_name, prefix="assets"))
    mime_type = models.TextField(choices=MIME_TYPE_CHOICES)

    class Meta:
        constraints = [models.UniqueConstraint(name="django_secretary_asset_unique_name", fields=["template", "name"])]


class Tag(models.Model):
    """Represent a tag.

    Attributes:
        name: Name of the tag.
    """

    name = models.TextField(unique=True)

    def __str__(self) -> str:
        return "{}".format(self.name)


class TemplateCollection(models.Model):
    """Represent a template collection.

    Attributes:
        name: Name of the collection.
        tags: Collection tags.
    """

    name = models.TextField(unique=True)
    label = models.TextField()
    tags = models.ManyToManyField(Tag)  # type: ignore[var-annotated]
