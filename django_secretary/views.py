"""REST API for django-secretary."""

from typing import Any, List, cast

from django.utils.functional import SimpleLazyObject
from django_filters import ModelChoiceFilter
from django_filters.rest_framework import DjangoFilterBackend, FilterSet
from rest_framework.decorators import action
from rest_framework.filters import OrderingFilter
from rest_framework.pagination import PageNumberPagination
from rest_framework.renderers import BaseRenderer
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.routers import DefaultRouter
from rest_framework.serializers import JSONField, ModelSerializer, Serializer, SlugRelatedField
from rest_framework.settings import api_settings
from rest_framework.viewsets import ReadOnlyModelViewSet

from .models import Asset, Tag, Template, TemplateCollection
from .settings import SECRETARY_SETTINGS


class DefaultPagination(PageNumberPagination):
    """Set the default list size."""

    page_size = api_settings.PAGE_SIZE or 1000


class TemplateSerializer(ModelSerializer):
    """Template serializer."""

    # Use UUID for relation to parent, requires `slug_field` defined in `extra_kwargs`.
    serializer_related_field = SlugRelatedField

    class Meta:
        model = Template
        fields = ("name", "is_active", "uuid", "create_datetime", "parent", "asset_set")
        extra_kwargs = {"parent": {"slug_field": "uuid"}, "asset_set": {"slug_field": "uuid"}}


class TemplateRenderSerializer(Serializer):
    """Serializer for template rendering data."""

    context = JSONField(required=False, default={})


class AssetSerializer(ModelSerializer):
    """Asset serializer."""

    # Use UUID for relation to template, requires `slug_field` defined in `extra_kwargs`.
    serializer_related_field = SlugRelatedField

    class Meta:
        model = Asset
        fields = ("name", "uuid", "template", "mime_type")
        extra_kwargs = {"template": {"slug_field": "uuid"}}


class TemplateCollectionSerializer(ModelSerializer):
    """Template collection serializer."""

    # Use name for relation to tags, requires `slug_field` defined in `extra_kwargs`.
    serializer_related_field = SlugRelatedField

    class Meta:
        model = TemplateCollection
        fields = ("name", "label", "tags")
        extra_kwargs = {"tags": {"slug_field": "name"}}


# Wrap renderers from settings into a lazy object, to allow its changes in tests.
def _get_renderers() -> List[BaseRenderer]:
    return cast(List[BaseRenderer], SECRETARY_SETTINGS.RENDERERS)


_RENDERERS = SimpleLazyObject(_get_renderers)


class TemplateViewSet(ReadOnlyModelViewSet):
    """Template view set."""

    queryset = Template.objects.all()
    serializer_class = TemplateSerializer
    lookup_field = "uuid"
    filter_backends = [OrderingFilter]
    ordering_fields = ["name", "create_datetime"]
    ordering = ["name"]
    paginator = DefaultPagination()

    @action(methods=["POST"], detail=True, renderer_classes=_RENDERERS)
    def render(self, request: Request, **kwargs: Any) -> Response:
        """Render the template with provided content and return a PDF."""
        serializer = TemplateRenderSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data["context"])


class ActiveTemplateViewSet(TemplateViewSet):
    """Template view set for active templates only."""

    queryset = Template.objects.filter(is_active=True)
    lookup_field = "name"
    # Default in rest_framework is '[^/.]+', we allow '.' in template name.
    lookup_value_regex = "[^/]+"


class AssetViewSet(ReadOnlyModelViewSet):
    """Asset view set."""

    queryset = Asset.objects.all()
    serializer_class = AssetSerializer
    lookup_field = "uuid"
    filter_backends = [OrderingFilter]
    ordering_fields = ["name"]
    ordering = ["name"]
    paginator = DefaultPagination()


class TemplateCollectionFilterSet(FilterSet):
    """Filters for template collections."""

    # Allow to filter by only one tag.
    tags = ModelChoiceFilter(to_field_name="name", queryset=Tag.objects.all())

    class Meta:
        model = TemplateCollection
        fields = ("tags",)


class TemplateCollectionViewSet(ReadOnlyModelViewSet):
    """Template collection view set."""

    queryset = TemplateCollection.objects.all()
    serializer_class = TemplateCollectionSerializer
    lookup_field = "name"
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = TemplateCollectionFilterSet
    ordering_fields = ["name"]
    ordering = ["name"]
    paginator = DefaultPagination()


ROUTER = DefaultRouter()
ROUTER.register("templates/active", ActiveTemplateViewSet, basename="active-template")
ROUTER.register("templates", TemplateViewSet)
ROUTER.register("assets", AssetViewSet)
ROUTER.register("template-collections", TemplateCollectionViewSet)
