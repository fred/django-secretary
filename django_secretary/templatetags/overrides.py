"""Template tags and filters overrides for Djangos' ones."""

from decimal import Decimal, InvalidOperation
from typing import Union

from django import template
from django.template.defaultfilters import floatformat as _floatformat

register = template.Library()


class _DecimalWrapper:
    """Utility wrapper to pass decimal value into a Django's floatformat filter."""

    def __init__(self, value: Decimal):
        self.value = value

    def __repr__(self) -> str:
        return str(self.value)


@register.filter(is_safe=True)
def floatformat(value: Union[str, Decimal, float], arg: Union[str, int] = -1) -> str:
    """Display a number to a specified number of decimal places.

    Overrides Django implementation to avoid loss of precision in decimal numbers,
    see https://github.com/django/django/pull/15863.
    """
    wrapped: Union[str, Decimal, float, _DecimalWrapper] = value
    if isinstance(value, str):
        try:
            wrapped = _DecimalWrapper(Decimal(value))
        except (ValueError, InvalidOperation):
            # Fallback to vanilla floatformat.
            pass
    elif isinstance(value, Decimal):
        wrapped = _DecimalWrapper(value)
    return _floatformat(wrapped, arg)
