"""Template tags and filters for text manipulation."""

import re
from itertools import zip_longest

from django import template
from django.template.defaultfilters import linebreaksbr
from jinja2.environment import Environment
from jinja2.ext import Extension

register = template.Library()


_WHITESPACE_RE = re.compile(r"\s")


@register.filter
def forcewrap(value: str, length: int = 70) -> str:
    """Return a string with all whitespaces removed and split to lines of defined length."""
    raw = _WHITESPACE_RE.sub("", value)
    # Based on recipe for `grouper` from itertools.
    args = [iter(raw)] * length
    return "\n".join("".join(c) for c in zip_longest(*args, fillvalue=""))


class TextUtilsExtension(Extension):
    """A Jinja2 extension with template filters for text manipulation."""

    def __init__(self, environment: Environment):
        super().__init__(environment)

        environment.filters["forcewrap"] = forcewrap
        environment.filters["linebreaksbr"] = linebreaksbr
