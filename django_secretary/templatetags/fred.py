"""Template tags and filters for FRED objects."""

from typing import Dict, Optional, cast

from django import template
from jinja2.environment import Environment
from jinja2.ext import Extension

from ..constants import DnskeyAlgorithm, DnskeyFlag, DnskeyProtocol
from ..settings import SECRETARY_SETTINGS

register = template.Library()


@register.filter
def parse_dnskey_algorithm(value: int) -> Optional[DnskeyAlgorithm]:  # type: ignore[valid-type]
    """Return parsed DNSKEY algorithm."""
    try:
        return DnskeyAlgorithm(value)  # type: ignore[operator, no-any-return]
    except ValueError:
        return None


@register.filter
def parse_dnskey_flags(value: int) -> Optional[DnskeyFlag]:  # type: ignore[valid-type]
    """Return parsed DNSKEY flags."""
    try:
        return DnskeyFlag(value)  # type: ignore[operator, no-any-return]
    except ValueError:
        return None


@register.filter
def parse_dnskey_protocol(value: int) -> Optional[DnskeyProtocol]:  # type: ignore[valid-type]
    """Return parsed DNSKEY algorithm."""
    try:
        return DnskeyProtocol(value)  # type: ignore[operator, no-any-return]
    except ValueError:
        return None


@register.filter
def state_flag_description(value: str) -> str:
    """Return description of the state flag."""
    return cast(Dict[str, str], SECRETARY_SETTINGS.STATE_FLAGS_DESCRIPTIONS).get(value, value)


class FredExtension(Extension):
    """A Jinja2 extension with FRED-specific template filters."""

    def __init__(self, environment: Environment):
        super().__init__(environment)

        environment.filters["parse_dnskey_algorithm"] = parse_dnskey_algorithm
        environment.filters["parse_dnskey_flags"] = parse_dnskey_flags
        environment.filters["parse_dnskey_protocol"] = parse_dnskey_protocol
        environment.filters["state_flag_description"] = state_flag_description
