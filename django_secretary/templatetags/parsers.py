"""Template tags and filters for parsing."""

from datetime import date, datetime
from typing import Optional

from django import template
from django.utils.dateparse import parse_date as _parse_date, parse_datetime as _parse_datetime
from jinja2.environment import Environment
from jinja2.ext import Extension

register = template.Library()


@register.filter
def parse_date(value: str) -> Optional[date]:
    """Parse date from a string."""
    try:
        return _parse_date(value)
    except (TypeError, ValueError):
        return None


@register.filter
def parse_datetime(value: str) -> Optional[datetime]:
    """Parse datetime from a string."""
    try:
        return _parse_datetime(value)
    except (TypeError, ValueError):
        return None


class ParsersExtension(Extension):
    """A Jinja2 extension with template filters for parsing data."""

    def __init__(self, environment: Environment):
        super().__init__(environment)

        environment.filters["parse_date"] = parse_date
        environment.filters["parse_datetime"] = parse_datetime
