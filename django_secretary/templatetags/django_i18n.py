"""Template tags and filters which migrates Django's i18n templatetags module to Jinja2 extension."""

from typing import Any, List, Union, cast

from django.utils import translation
from jinja2.environment import Environment
from jinja2.ext import Extension
from jinja2.nodes import Call, ExtensionAttribute, Node, Output
from jinja2.parser import Parser
from jinja2.runtime import Macro
from jinja2_simple_tags import ContainerTag, StandaloneTag


class GetCurrentLanguage(StandaloneTag):
    """Jinja2 version of get_current_language template tag."""

    tags = {"get_current_language"}

    def render(self) -> str:
        """Return the language code."""
        return translation.get_language()


class Language(ContainerTag):
    """Jinja2 version of language template tag."""

    tags = {"language"}

    def render(self, language_name: str, caller: Macro) -> str:
        """Render content in defined language."""
        with translation.override(language_name):
            return caller()


class DjangoI18nExtension(Extension):
    """A Jinja2 extension with template tags and filters for i18n."""

    tags = GetCurrentLanguage.tags | Language.tags

    def __init__(self, environment: Environment):
        super().__init__(environment)
        self._get_current_language = GetCurrentLanguage(environment)
        self._language = Language(environment)

    def parse(self, parser: Parser) -> Union[Node, List[Node]]:
        """Parse the extension specific part of the stream."""
        current_tag = parser.stream.current.value
        if current_tag in GetCurrentLanguage.tags:
            nodes = cast(Output, self._get_current_language.parse(parser))
            target_node = cast(ExtensionAttribute, [n for n in nodes.iter_child_nodes() if isinstance(n, Call)][0].node)
            assert target_node.identifier == self._get_current_language.identifier  # noqa: S101
            assert target_node.name == "render_wrapper"  # noqa: S101
            wrapper_name = "_render_get_current_language"
        else:
            assert current_tag in Language.tags  # noqa: S101
            nodes = cast(Output, self._language.parse(parser))
            target_node = cast(ExtensionAttribute, [n for n in nodes.iter_child_nodes() if isinstance(n, Call)][0].node)
            assert target_node.identifier == self._language.identifier  # noqa: S101
            assert target_node.name == "render_wrapper"  # noqa: S101
            wrapper_name = "_render_language"
        target_node.identifier = self.identifier
        target_node.name = wrapper_name
        return nodes

    def _render_get_current_language(self, *args: Any, **kwargs: Any) -> str:
        return cast(str, self._get_current_language.render_wrapper(*args, **kwargs))

    def _render_language(self, *args: Any, **kwargs: Any) -> str:
        return cast(str, self._language.render_wrapper(*args, **kwargs))
