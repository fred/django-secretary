"""Template tags and filters which migrates Django's l10n templatetags module to Jinja2 extension."""

from django.template.defaultfilters import date
from django.templatetags.l10n import localize
from django.templatetags.tz import localtime
from jinja2.environment import Environment
from jinja2.ext import Extension

from .overrides import floatformat


class DjangoL10nExtension(Extension):
    """A Jinja2 extension with template tags and filters for l10n."""

    def __init__(self, environment: Environment):
        super().__init__(environment)

        environment.filters["date"] = date
        environment.filters["floatformat"] = floatformat
        environment.filters["localize"] = localize
        environment.filters["localtime"] = localtime
