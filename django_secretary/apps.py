"""AppConfig definition."""

from typing import Any

from django.apps import AppConfig
from django.test.signals import setting_changed
from django.utils.translation import gettext_lazy as _

from .settings import SECRETARY_SETTINGS


def _reset_template_engines(**kwargs: Any) -> None:
    """Clean all template engine caches."""
    if kwargs["setting"] != "SECRETARY_TEMPLATES":  # pragma: no cover
        return
    # Based on django.test.signals.reset_template_engines
    from .utils.templates import TEMPLATE_ENGINES

    try:
        del TEMPLATE_ENGINES.templates
    except AttributeError:
        pass
    # Ignore typing, because we use private attributes here.
    TEMPLATE_ENGINES._templates = SECRETARY_SETTINGS.TEMPLATES  # type: ignore[attr-defined]
    TEMPLATE_ENGINES._engines = {}  # type: ignore[attr-defined]
    from django.template.engine import Engine

    Engine.get_default.cache_clear()  # type: ignore[attr-defined]


class SecretaryAppConfig(AppConfig):
    """Django-secretary application config."""

    name = "django_secretary"
    verbose_name = _("Secretary")
    default_auto_field = "django.db.models.BigAutoField"

    def ready(self) -> None:
        """Check settings."""
        SECRETARY_SETTINGS.check()
        setting_changed.connect(SECRETARY_SETTINGS.invalidate_cache)
        setting_changed.connect(_reset_template_engines)
