"""Template utilities for django-secretary."""

from contextlib import ContextDecorator
from threading import local
from types import TracebackType
from typing import Any, Callable, Dict, Iterator, List, Optional, Tuple, Type, cast

from django.http.request import HttpRequest
from django.template import EngineHandler, Origin, TemplateDoesNotExist
from django.template.backends.base import BaseEngine
from django.template.backends.django import DjangoTemplates
from django.template.loaders.base import Loader
from django.utils.functional import cached_property
from django.utils.module_loading import import_string
from jinja2.environment import Environment
from jinja2.loaders import BaseLoader as JinjaBaseLoader

from ..models import Template as TemplateModel, get_template_from_uid
from ..protocols import Engine, Template
from ..settings import SECRETARY_SETTINGS

TEMPLATE_ENGINES = EngineHandler(SECRETARY_SETTINGS.TEMPLATES)

# Thread-safe storage of active templates.
# If the attribute `template` exists, it contains the actual active template.
# If the attribute `templates` exists, it contains mapping of template name to `Template` model instance.
_ACTIVE_TEMPLATES = local()


def get_active_template(name: Optional[str] = None) -> TemplateModel:
    """Return active template or raise an exception.

    Arguments:
        name: If provided, return one of the active templates matching the name.
              If missing, return the active template itself.

    Raises:
        TemplateDoesNotExist: If template was not found.
    """
    if name is None:
        if hasattr(_ACTIVE_TEMPLATES, "template"):
            return cast(TemplateModel, _ACTIVE_TEMPLATES.template)
        else:
            raise TemplateDoesNotExist("There is no active template.")

    if hasattr(_ACTIVE_TEMPLATES, "templates"):
        if name in _ACTIVE_TEMPLATES.templates:
            return cast(TemplateModel, _ACTIVE_TEMPLATES.templates[name])
        raise TemplateDoesNotExist("'{}' has no active template.".format(name))
    try:
        return TemplateModel.objects.get(name=name, is_active=True)
    except TemplateModel.DoesNotExist as error:
        raise TemplateDoesNotExist("'{}' is neither active, nor found in database.".format(name)) from error


class override_template(ContextDecorator):
    """Context manager/decorator to force active template."""

    def __init__(self, template: TemplateModel):
        self.template = template
        self._old_template: Optional[TemplateModel] = None
        self._old_templates: Optional[Dict[str, TemplateModel]] = None

    def __enter__(self) -> None:
        self._old_template = getattr(_ACTIVE_TEMPLATES, "template", None)
        self._old_templates = getattr(_ACTIVE_TEMPLATES, "templates", None)

        active_templates = {}
        template: Optional[TemplateModel] = self.template
        while template is not None:
            active_templates[template.name] = template
            template = template.parent
        _ACTIVE_TEMPLATES.template = self.template
        _ACTIVE_TEMPLATES.templates = active_templates

    def __exit__(
        self,
        exc_type: Optional[Type[BaseException]],
        exc_value: Optional[BaseException],
        traceback: Optional[TracebackType],
    ) -> None:
        if self._old_template is None:
            del _ACTIVE_TEMPLATES.template
            del _ACTIVE_TEMPLATES.templates
        else:
            _ACTIVE_TEMPLATES.template = self._old_template
            _ACTIVE_TEMPLATES.templates = self._old_templates


class TemplateLoader(Loader):
    """Template loader which returns active templates from `Template` model."""

    def get_template_sources(self, template_name: str) -> Iterator[Origin]:
        """Return template origin."""
        yield Origin(name=template_name, template_name=template_name, loader=self)

    def get_contents(self, origin: Origin) -> str:
        """Return template contents or raise an exception.

        Raises:
            TemplateDoesNotExist: If template was not found.
        """
        template_file = get_active_template(origin.name).content
        try:
            if not template_file:
                raise TemplateDoesNotExist(
                    origin.name,
                    backend=cast(BaseEngine, self.engine),
                    tried=[(origin, f"Template {origin.name} does not support Django templates.")],
                )
            return cast(bytes, template_file.read()).decode(encoding=self.engine.file_charset)
        finally:
            template_file.close()


def secretary_environment(loader: str, **params: Any) -> Environment:
    """Return a secretary-specific Jinja2 environment.

    Arguments:
        loader: Dotted path to a loader class.
        **params: Additional parameters to be passed to the `jinja2.environment.Environment`.
    """
    loader_obj = import_string(loader)
    env = Environment(loader=loader_obj(), **params)
    return env


class JinjaTemplateLoader(JinjaBaseLoader):
    """A loader of templates for `Jinja`."""

    def __init__(self, encoding: str = "utf-8") -> None:
        """Initialize loader."""
        self.encoding = encoding

    def get_source(self, environment: Environment, template: str) -> Tuple[str, str, Callable[[], bool]]:
        """Get source of the template, given by its `uid`.

        Raises `Template.DoesNotExist` if template is not found.
        """
        try:
            template_obj = get_template_from_uid(template)
        except Exception as e:
            raise TemplateDoesNotExist(f"Template {template} does not exist.") from e

        template_file = template_obj.jinja_content
        try:
            if not template_file:
                raise TemplateDoesNotExist(f"Template {template} does not support Jinja2 templates.")
            content = cast(bytes, template_file.read()).decode(encoding=self.encoding)
        finally:
            template_file.close()
        return content, template_obj.name, lambda: False  # always reload the template


def _get_template(template_name_fn: Callable[[Engine], str], engines: List[Engine]) -> Template:
    """Return a template object by the first engine that finds it.

    This is a re-implemented version of the `get_template` function from `django.template.loader`.
    The original function doesn't suit our needs as we need the template name to be dynamic,
    and the template engines to be in an order defined by the template model.

    Arguments:
        template_name_fn: generate the template name to look for, based on the currently considered engine.
        engines: a list of engines to try.

    Returns: a template object.

    Raises: A TemplateDoesNotExist exception if the template isn't found by any of the engines.
    """
    chain = []
    for engine in engines:
        try:
            template = engine.get_template(template_name_fn(engine))
            if template is None:
                raise TemplateDoesNotExist(
                    f"Template {template_name_fn(engine)} wasn't found by {engine}",
                    backend=engine,  # type: ignore[arg-type]
                )
            return template
        except TemplateDoesNotExist as e:
            chain.append(e)

    raise TemplateDoesNotExist(template_name_fn(engine), chain=chain)


class TemplateWrapper:
    """A wrapper around engine template, such as django.template.backends.django.Template.

    Adds the template model to the API.
    """

    def __init__(self, model: TemplateModel, engines: List[Engine]) -> None:
        self.model = model
        self.engines = engines

    @cached_property
    def template(self) -> Template:
        """Return the wrapped engine template."""
        with override_template(self.model):
            return _get_template(
                # the `DjangoTemplates` engine looks for templates using their `name`, the rest use `uid`.
                lambda engine: self.model.name if isinstance(engine, DjangoTemplates) else self.model.uid,
                self.engines,
            )

    def render(self, context: Optional[Dict[str, Any]] = None, request: Optional[HttpRequest] = None) -> str:
        """Render the template with provided context.

        Arguments:
            context: Context data to be rendered.
            request: The request's additional information.
        """
        # Make a shallow copy to prevent unwated changes in provided context.
        context = context.copy() if context else {}
        context["template"] = self.model
        with override_template(self.model):
            return self.template.render(context, request)
