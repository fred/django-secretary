"""Custom fetchers for weasyprint."""

import mimetypes
import os
import ssl
from typing import TYPE_CHECKING, Any, Dict, Optional, cast
from urllib.parse import urljoin, urlsplit, urlunsplit

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.core.files.storage import default_storage
from django.template import TemplateDoesNotExist

if TYPE_CHECKING:
    from ..models import Asset


class FileFetcher:
    """Fetcher which only fetches files from file system.

    Attributes:
        media_url: Base URL of the media files. All served media files must belong to it.
    """

    def __init__(self, base_url: str):
        """Initialize fetcher."""
        # TODO: Only possible in Django <= 3.0.
        if not settings.MEDIA_URL:  # pragma: no cover
            raise ImproperlyConfigured("Django-secretary fetcher requires non-empty MEDIA_URL.")
        if not settings.MEDIA_ROOT:
            raise ImproperlyConfigured("Django-secretary fetcher requires non-empty MEDIA_ROOT.")
        self.media_url = urljoin(base_url, settings.MEDIA_URL)

    def _get_asset(self, asset_name: str) -> Optional["Asset"]:
        """Return the asset or None."""
        from ..models import Asset, Template
        from .templates import get_active_template

        # Try template assets
        try:
            template: Optional[Template] = get_active_template()
        except TemplateDoesNotExist:
            pass
        else:
            while template is not None:
                try:
                    return cast(Asset, template.asset_set.get(name=asset_name))
                except Asset.DoesNotExist:
                    # Asset not found, try parent.
                    template = template.parent

        # Try global assets
        try:
            return Asset.objects.get(name=asset_name, template=None)
        except Asset.DoesNotExist:
            pass

        # Asset not found
        return None

    def fetch(self, url: str, timeout: int = 10, ssl_context: Optional[ssl.SSLContext] = None) -> Dict[str, Any]:
        """Return file based on URL."""
        url = _fix_doubleslashes(url)
        split_url = urlsplit(url)
        split_media_url = urlsplit(self.media_url)
        if (
            split_media_url.scheme == split_url.scheme
            and split_media_url.netloc == split_url.netloc
            and split_url.path.startswith(split_media_url.path)
        ):
            asset_name = split_url.path[len(split_media_url.path) :]
            asset = self._get_asset(asset_name)
            if asset is not None:
                return {"file_obj": asset.content, "mime_type": asset.mime_type, "redirected_url": url}

            filename = os.path.join(settings.MEDIA_ROOT, *asset_name.split("/"))
            # Try direct access
            try:
                data = default_storage.open(filename)
            except FileNotFoundError:
                pass
            else:
                return {"file_obj": data, "mime_type": mimetypes.guess_type(url)[0], "redirected_url": url}
            raise ValueError("Media file {} not found at {}.".format(url, filename))
        raise ValueError("URI {} is out of base URL.".format(url))


def _fix_doubleslashes(url: str) -> str:
    """Return an url, where all the doubleslashes in `path` were resolved."""
    scheme, netloc, path, query, fragment = urlsplit(url)
    while "//" in path:
        path = path.replace("//", "/")
    return urlunsplit((scheme, netloc, path, query, fragment))
