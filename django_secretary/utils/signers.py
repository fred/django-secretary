"""Utilities for PDF signatures."""

import os
import subprocess
from io import BytesIO
from tempfile import TemporaryDirectory
from typing import Any, BinaryIO, Dict, cast

from pyhanko.pdf_utils.font.opentype import GlyphAccumulatorFactory
from pyhanko.pdf_utils.images import PdfImage
from pyhanko.pdf_utils.incremental_writer import IncrementalPdfFileWriter
from pyhanko.pdf_utils.text import TextBoxStyle
from pyhanko.sign.fields import SigFieldSpec, append_signature_field
from pyhanko.sign.signers import PdfSignatureMetadata, PdfSigner, SimpleSigner
from pyhanko.stamp import TextStampStyle

from ..settings import SECRETARY_SETTINGS


class SignerNotFound(Exception):
    """Signer not found."""


def _sign_pdf_command(signer_setting: Dict[str, Any], pdf: bytes) -> bytes:
    """Actually sign PDF using command."""
    with TemporaryDirectory() as tmp_dir:
        with open(os.path.join(tmp_dir, "input.pdf"), "wb") as file:
            file.write(pdf)
        output = signer_setting["OUTPUT"]
        cmd = [i.format(output=output) for i in signer_setting["COMMAND"]]
        try:
            subprocess.run(  # noqa: S603
                cmd,
                cwd=tmp_dir,
                check=True,
                capture_output=True,
                env=signer_setting["ENVIRONMENT"],
            )
        except subprocess.CalledProcessError as error:
            raise RuntimeError("{} Output: {}, error: {}".format(error, error.stdout, error.stderr)) from error
        with open(os.path.join(tmp_dir, output), "rb") as file:
            return file.read()


_PYHANKO_FIELD_NAME = "Signature"


def _sign_pdf_pyhanko(signer_setting: Dict[str, Any], pdf: bytes) -> bytes:
    """Actually sign PDF using pyHanko."""
    password = signer_setting.get("PASSWORD")
    if password is not None:
        # Password has to be in bytes.
        password = password.encode()
    pdf_signer = SimpleSigner.load_pkcs12(signer_setting["CERTIFICATE"], passphrase=password)
    writer = IncrementalPdfFileWriter(BytesIO(pdf))
    if "BOX" in signer_setting:
        append_signature_field(writer, sig_field_spec=SigFieldSpec(_PYHANKO_FIELD_NAME, box=signer_setting["BOX"]))

    box_style: Dict[str, Any] = {}
    if signer_setting["FONT_FILE"]:
        box_style["font"] = GlyphAccumulatorFactory(signer_setting["FONT_FILE"])

    style = {
        "border_width": signer_setting["BORDER_WIDTH"],
        "stamp_text": signer_setting["TEXT"],
        "text_box_style": TextBoxStyle(font_size=signer_setting["FONT_SIZE"], **box_style),
    }
    if signer_setting["IMAGE"] is not None:
        style["background"] = PdfImage(signer_setting["IMAGE"])

    metadata = PdfSignatureMetadata(field_name=_PYHANKO_FIELD_NAME)
    out = cast(BinaryIO, PdfSigner(metadata, pdf_signer, stamp_style=TextStampStyle(**style)).sign_pdf(writer))
    return out.read()


def sign_pdf(signer: str, pdf: bytes) -> bytes:
    """Return the PDF signed by the signer."""
    if signer not in SECRETARY_SETTINGS.SIGNERS:
        raise SignerNotFound("Signer '{}' not found.".format(signer))

    signer_setting = SECRETARY_SETTINGS.SIGNERS[signer]
    if signer_setting["METHOD"] == "command":
        return _sign_pdf_command(signer_setting, pdf)
    else:
        assert signer_setting["METHOD"] == "pyhanko"  # noqa: S101
        return _sign_pdf_pyhanko(signer_setting, pdf)
